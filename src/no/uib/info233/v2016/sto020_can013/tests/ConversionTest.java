// Class to use in a Junit test.

package no.uib.info233.v2016.sto020_can013.tests;

/**
 * @author can013
 * @author sto020
 */

public class ConversionTest {
	
	
	/*
	 * Constructor for ConversionTest.
	 */
	public ConversionTest() {
		
	}
	
	/**
	 * @param string the input string to be checked.
	 * @return priority the result of the priority variable.
	 * 
	 * Method to parse input string as an integer and checks
	 * to find out which priority status to be assigned to the
	 * integer.
	 * 
	 */
	public String conversionPriority(String string) {
		String priority = null;
		int tempInt = Integer.parseInt(string.trim());
		if (tempInt <= 10) priority = "None";
		if (tempInt > 10 && tempInt <= 25) priority = "Low";
		if (tempInt > 25 && tempInt <= 60) priority = "Normal";
		if (tempInt > 60 && tempInt <= 85) priority = "High";
		if (tempInt > 85 && tempInt <= 100) priority = "Critical";
		
		return priority;
	}
	
}
