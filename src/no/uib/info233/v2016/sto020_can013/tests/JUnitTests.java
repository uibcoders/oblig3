// JUnit class to perform tests.

package no.uib.info233.v2016.sto020_can013.tests;

import static org.junit.Assert.assertEquals;

import java.io.File;

import no.uib.info233.v2016.sto020_can013.oblig3.XML.XMLBuilder;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;

import org.junit.Test;

/**
 * @author can013
 * @author sto020
 */
public class JUnitTests {
	
	/**
	 * Test to check that all possible outcomes of conversion
	 * from int to a predetermined string are correct.
	 * 
	 */
	@Test
	public void intConversionTest() {
		ConversionTest con = new ConversionTest();
		String result = con.conversionPriority(" 2"); // result equal to the returned string of 
		String result1 = con.conversionPriority("15");
		String result2 = con.conversionPriority(" 35");
		String result3 = con.conversionPriority(" 70");
		String result4 = con.conversionPriority("100");
		assertEquals("None", result); // checking if result string equals the expected result.
		assertEquals("Low", result1);
		assertEquals("Normal", result2);
		assertEquals("High", result3);
		assertEquals("Critical", result4);
		
	}
	
	/*
	 * Test to check if a file contains the expected 
	 * information.
	 */
	@Test
	public void issueBuilderTest() {
		IssueTracker iTracker = Utils.loadData();
		
		assertEquals(true, XMLBuilder.issueBuilder(iTracker.getIssueList(), new File(".//old_issues.xml")));
	}
	
	

}
