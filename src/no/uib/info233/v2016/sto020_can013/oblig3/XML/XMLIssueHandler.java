/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig3.XML;

import java.time.LocalDate;

import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;

/**
 * Handles the data for the issue tables
 * Splits the data from the issues and adds them to the correct place in the tabledata
 * @author sto020
 * @author can013
 */
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;
/**
 * @author sto020 
 * This class handles the import of a set of Issues saved to an
 *         XML file
 *
 */
public class XMLIssueHandler extends DefaultHandler {
	private boolean ID = false;
	private boolean priority = false;
	private boolean assigned_user = false;
	private boolean created = false;
	private boolean text = false;
	private boolean location = false;
	private boolean status = false;
	private boolean createdBy = false;
	private boolean changedBy = false;

	private String ID_string = null;
	private String priority_string = null;
	private String assigned_user_string = null;
	private LocalDate created_date = null;
	private String text_string = null;
	private String location_string = null;
	private String status_string = null;
	private String createdBy_string = null;
	private String changedBy_string = null;
	private IssueTracker iTracker = null;

	public XMLIssueHandler(IssueTracker iTracker) {
		this.iTracker = iTracker;

	}

	/*
	 * Method to apply values from attributes to field variables to be used to
	 * identify columns.
	 * 
	 * @param uri
	 * 
	 * @param localName
	 * 
	 * @param qName
	 * 
	 * @param attributes
	 */

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		//Checks what data exist inside the issue field of th XML file
		if (qName.equalsIgnoreCase("issue")) {
			ID_string = attributes.getValue("id");
			if (!ID_string.equals(null))
				ID = true;
		}

		if (qName.equalsIgnoreCase("priority")) {
			priority = true;
		}
		if (qName.equalsIgnoreCase("assigned_user")) {
			assigned_user = true;
		}

		if (qName.equalsIgnoreCase("created")) {
			created = true;
		}
		if (qName.equalsIgnoreCase("text")) {
			text = true;
		}
		if (qName.equalsIgnoreCase("location")) {
			location = true;
		}
		if (qName.equalsIgnoreCase("status")) {
			status = true;
		}
		if (qName.equalsIgnoreCase("createdBy")) {
			createdBy = true;
		}
		if (qName.equalsIgnoreCase("changedBy")) {
			changedBy = true;
		}
	}

	public void characters(char ch[], int start, int length) throws SAXException {
		
		//Gets the data from the XML file if it exists
		if (ID) {
			// System.out.println("ID : " + ID_string);
			ID = false;
		}

		if (priority) {
			priority_string = new String(ch, start, length);
			priority = false;
		}

		if (assigned_user) {
			assigned_user_string = new String(ch, start, length);
			assigned_user = false;
		}

		if (created) {
			String date = new String(ch, start, length);
			created_date = Utils.StringToLocalDate(date);
			created = false;
		}
		if (text) {
			text_string = new String(ch, start, length);
			text = false;
		}
		if (location) {
			location_string = new String(ch, start, length);
			location = false;
		}
		if (status) {
			status_string = new String(ch, start, length);
			status = false;
		}
		if (createdBy) {
			createdBy_string = new String(ch, start, length);
			createdBy = false;
		}
		if (changedBy) {
			changedBy_string = new String(ch, start, length);
			changedBy = false;
		}

	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		
		//Checks if it is the last element of the issue, and then adds it to the Tracker
		if (qName.equalsIgnoreCase("issue")) {

			iTracker.addIssue(Utils.StringToInteger(ID_string), priority_string, iTracker.getUser(assigned_user_string),
					created_date, text_string, location_string, status_string, createdBy_string, changedBy_string);

			//reset variables
			ID = false;
			priority = false;
			assigned_user = false;
			created = false;
			text = false;
			location = false;
			status = false;
			createdBy = false;
			changedBy = false;

			ID_string = null;
			priority_string = null;
			assigned_user_string = null;
			created_date = null;
			text_string = null;
			location_string = null;
			status_string = null;
			createdBy_string = null;
			changedBy_string = null;

		}

	}

}