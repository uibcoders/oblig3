
package no.uib.info233.v2016.sto020_can013.oblig3.XML;

import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.User;
import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;

/**
 * Handles the data for the issue tables
 * Splits the data from the issues and adds them to the correct place in the tabledata
 * @author sto020
 * @author can013
 */
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author sto020
 * This class handles the import of a set of Users saved to an XML file
 *
 */
public class XMLUserHandler extends DefaultHandler {
	private boolean ID = false; // boolean ID set to be false.
	private boolean username = false;
	private boolean firstname = false;
	private boolean lastname = false;
	private boolean password = false;

	private String ID_string = null; // Field for ID
	private String username_string = null;
	private String firstname_string = null;
	private String lastname_string = null;
	private String password_string = null;

	private IssueTracker iTracker = null; // IssueTracker field to be used.

	
	/**
	 * Constructor for the class.
	 * @param iTracker input to be the value of field variable iTracker.
	 */
	public XMLUserHandler(IssueTracker iTracker) {
		this.iTracker = iTracker;

	}

	/*
	 * Method to apply values from attributes to field variables to be used to
	 * identify columns.
	 * 
	 * @param uri
	 * 
	 * @param localName
	 * 
	 * @param qName
	 * 
	 * @param attributes
	 */

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		//Checks to see what variables are saved to the XML user and checks them off.
		if (qName.equalsIgnoreCase("user")) {
			ID_string = attributes.getValue("id");
			if (!ID_string.equals(null))
				ID = true;
		}
		if (qName.equalsIgnoreCase("username")) {
			username = true;

		}
		if (qName.equalsIgnoreCase("firstname")) {
			firstname = true;
		}

		if (qName.equalsIgnoreCase("lastname")) {
			lastname = true;
		}
		if (qName.equalsIgnoreCase("password")) {
			password = true;
		}

	}

	public void characters(char ch[], int start, int length) throws SAXException {
		//Gets the data tied to the variables from the XML file
		if (ID) {
			// System.out.println("ID : " + ID_string);
			ID = false;
		}
		if (username) {

			username_string = new String(ch, start, length);

			username = false;
		}

		if (firstname) {
			firstname_string = new String(ch, start, length);
			firstname = false;
		}

		if (lastname) {
			lastname_string = new String(ch, start, length);
			lastname = false;
		}

		if (password) {
			password_string = new String(ch, start, length);
			password = false;
		}

	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
//Checks if the end element is the last element of the user, then it adds the user to the Tracker
		if (qName.equalsIgnoreCase("user")) {
			User user = new User(Utils.StringToInteger(ID_string), username_string);
			user.setFirstName(firstname_string);
			user.setLastName(lastname_string);
			user.setLoginPasswordHash(password_string);

			iTracker.addUser(user);
			
			//reset
			ID_string = null;
			username_string = null;
			firstname_string = null;
			lastname_string = null;
			password_string = null;

			ID = false;
			username = false;
			firstname = false;
			lastname = false;
			password = false;
		}

	}

}