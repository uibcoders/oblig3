/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig3.XML;

import java.time.LocalDate;

import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;

/**
 * Handles the data for the issue tables
 * Splits the data from the issues and adds them to the correct place in the tabledata
 * @author sto020
 * @author can013
 */
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;
/**
 * @author sto020 
 * @author can013
 * This class handles the import of a set of Issues saved to an out-dated XML database file
 *
 */
public class XMLImportHandler extends DefaultHandler {

	private String ID = null;
	private String priority = null;
	private String assigned_user = null;
	private LocalDate created = null;
	private String text = null;
	private String location = null;

	private IssueTracker iTracker = null;

	public XMLImportHandler(IssueTracker iTracker) {
		this.iTracker = iTracker;

	}

	/*
	 * Method to apply values from attributes to field variables to be used to
	 * identify columns.
	 * 
	 * @param uri
	 * 
	 * @param localName
	 * 
	 * @param qName
	 * 
	 * @param attributes
	 */

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (attributes.getLength() > 0) {

			for (int i = 0; i < attributes.getLength(); i++) {

				switch (attributes.getLocalName(i)) {
				case "id":
					ID = attributes.getValue(i);
					break;
				case "priority": // if it equals priority and satisfies an if
									// condition it sets the value to either
									// none/low/normal/high or critical.
					
					// parses the string as	 an	integer and trims the string.
					int tempInt = Integer.parseInt(attributes.getValue(i).trim()); 
					
					if (tempInt <= 10) {
						priority = "None";
						break;
					}
					if (tempInt > 10 && tempInt <= 25) {
						priority = "Low";
						break;
					}
					if (tempInt > 25 && tempInt <= 60) {
						priority = "Normal";
						break;
					}
					if (tempInt > 60 && tempInt <= 85) {
						priority = "High";
						break;
					}
					if (tempInt > 85 && tempInt <= 100) {
						priority = "Critical";
						break;
					} else {
						priority = "Unresolved priority";
					}
				case "assigned_user":
					assigned_user = attributes.getValue(i);
					break;
				case "created":
					created = Utils.StringToLocalDate(attributes.getValue(i));
					break;
				case "text":
					text = attributes.getValue(i);
					break;
				case "location":
					location = attributes.getValue(i);
					break;
				}

			}

			// add too issueList
			iTracker.addIssue(Utils.StringToInteger(ID), priority, iTracker.addUser(assigned_user), created, text,
					location, "Assigned Employee", "import", "import");

		}

	}

	public void characters(char ch[], int start, int length) throws SAXException {

		// not needed
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {

		// not needed
	}

}