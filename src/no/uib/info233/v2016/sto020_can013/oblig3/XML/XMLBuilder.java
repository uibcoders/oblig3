package no.uib.info233.v2016.sto020_can013.oblig3.XML;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.Issue;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.User;

/**
 * @author sto020
 * @author can013
 */

/*
 * Builds issues and users into an XML tree
 */

public class XMLBuilder {

	/**
	 * Creates an XML document with a tree of issues
	 * 
	 * Template of output
	 * <issues>
	 * <issue id="1"> 
	 * <priority>Critical</priority>
	 * <assigned_user type="username">dryan3</assigned_user>
	 * <created type="date">2016-02-03</created>
	 * <text type="description"></text>
	 * <location type="city">Kulon</location>
	 * <status>Assigned Employee</status>
	 * <createdBy>import</createdBy>
	 * <changedBy>import</changedBy>
	 * </issue>
	 * </issues>
	 * 
	 * @param issueList
	 *            The list of issues to save
	 * @param file
	 *            - the path to save the issues
	 * @return True if completed successfully
	 */
	public static boolean issueBuilder(List<Issue> issueList, File file) {
		if (issueList.isEmpty())
			return false;
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			// root element
			Element rootElement = doc.createElement("issues");
			doc.appendChild(rootElement);
			
			//loops through all issues and creates elements to be used in the xml output
			for (Issue currentIssue : issueList) {
				// System.out.println(currentIssue);
				// issue element
				Element issue = doc.createElement("issue");
				rootElement.appendChild(issue);

				// setting id to issue
				Attr attr = doc.createAttribute("id");
				attr.setValue(Integer.toString(currentIssue.getID()));
				issue.setAttributeNode(attr);

				if (currentIssue.getPriority() != null) {
					// priority element
					Element priority = doc.createElement("priority");
					priority.appendChild(doc.createTextNode(currentIssue.getPriority()));
					issue.appendChild(priority);
				}
				if (currentIssue.getAssignedUser() != null) {
					Element assigned_user = doc.createElement("assigned_user");
					Attr attrType1 = doc.createAttribute("type");
					attrType1.setValue("username");
					assigned_user.setAttributeNode(attrType1);
					assigned_user.appendChild(doc.createTextNode(currentIssue.getAssignedUser().getUsername()));
					issue.appendChild(assigned_user);
				}
				if (currentIssue.getCreatedDate() != null) {
					Element created = doc.createElement("created");
					Attr attrType2 = doc.createAttribute("type");
					attrType2.setValue("date");
					created.setAttributeNode(attrType2);
					created.appendChild(doc.createTextNode(currentIssue.getCreatedDate().toString()));
					issue.appendChild(created);
				}
				if (currentIssue.getDescriptiveText() != null) {
					Element description = doc.createElement("text");
					Attr attrType3 = doc.createAttribute("type");
					attrType3.setValue("description");
					description.setAttributeNode(attrType3);
					description.appendChild(doc.createTextNode(currentIssue.getDescriptiveText()));
					issue.appendChild(description);
				}

				if (currentIssue.getLocation() != null) {
					Element location = doc.createElement("location");
					Attr attrType4 = doc.createAttribute("type");
					attrType4.setValue("city");
					location.setAttributeNode(attrType4);
					location.appendChild(doc.createTextNode(currentIssue.getLocation()));
					issue.appendChild(location);
				}

				if (currentIssue.getStatus() != null) {
					Element status = doc.createElement("status");
					status.appendChild(doc.createTextNode(currentIssue.getStatus()));
					issue.appendChild(status);
				}

				if (currentIssue.getCreatedBy() != null) {
					Element createdBy = doc.createElement("createdBy");
					createdBy.appendChild(doc.createTextNode(currentIssue.getCreatedBy()));
					issue.appendChild(createdBy);
				}

				if (currentIssue.getChangedBy() != null) {
					Element changedBy = doc.createElement("changedBy");
					changedBy.appendChild(doc.createTextNode(currentIssue.getChangedBy()));
					issue.appendChild(changedBy);
				}

			}
			// write the content into xml file

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Creates an XML document with a tree of users
	 * Template of Output:
	 * <users>
	 * <user id="0">
	 * <firstname>Administrator</firstname>
	 * <lastname>Highest</lastname>
	 * <username>admin</username>
	 * <password>82c005ca99aac618063960224b1de280</password>
	 * </user>
	 * </users>
	 * 
	 * @param userList
	 *            The list of users to save
	 * @param file
	 *            - The path to save the users
	 * @return True if completed successfully
	 */
	public static boolean userBuilder(HashMap<String, User> userList, File file) {

		if (userList.isEmpty())
			return false;
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			// root element
			Element rootElement = doc.createElement("users");
			doc.appendChild(rootElement);

			//loops through all users and creates elements to be used in the xml output
			for (User currentUser : userList.values()) {
				// issue element
				Element user = doc.createElement("user");
				rootElement.appendChild(user);

				// setting id to user
				Attr attr = doc.createAttribute("id");
				attr.setValue(Integer.toString(currentUser.getID()));
				user.setAttributeNode(attr);

				if (currentUser.getFirstName() != null) {
					// firstname element
					Element firstname = doc.createElement("firstname");
					firstname.appendChild(doc.createTextNode(currentUser.getFirstName()));
					user.appendChild(firstname);
				}

				if (currentUser.getLastName() != null) {

					// lastname element
					Element lastname = doc.createElement("lastname");
					lastname.appendChild(doc.createTextNode(currentUser.getLastName()));
					user.appendChild(lastname);
				}

				// username element
				Element username = doc.createElement("username");
				username.appendChild(doc.createTextNode(currentUser.getUsername()));
				user.appendChild(username);

				if (currentUser.getLoginPasswordHash() != null) {
					// passwordhash element
					Element password = doc.createElement("password");
					password.appendChild(doc.createTextNode(currentUser.getLoginPasswordHash()));
					user.appendChild(password);
				}

			}
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);

			/*
			 * // Output to console for testing StreamResult consoleResult = new
			 * StreamResult(System.out); transformer.transform(source,
			 * consoleResult);
			 */

			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// e.printStackTrace();
		}
		return false;
	}
}