/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig3.XML;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;

/**
 * @author sto020
 * @author can013
 */
public class XMLReader {

	public static void readXML(File file, Object dh) {
		try {

			SAXParserFactory fac = SAXParserFactory.newInstance();
			SAXParser sParser = fac.newSAXParser();

			if (file.exists()) {
				InputStream inputStream = new FileInputStream(file);

				InputStreamReader inReader = new InputStreamReader(inputStream, "UTF-8");

				InputSource inSource = new InputSource(inReader);
				inSource.setEncoding("UTF-8");

				// checks which handler class has been passed to method
				if (dh.getClass() == XMLIssueHandler.class) {
					System.out.println("Loading issues");
					XMLIssueHandler handler = (XMLIssueHandler) dh;
					sParser.parse(inSource, handler);
				} else if (dh.getClass() == XMLUserHandler.class) {
					System.out.println("Loading users");
					XMLUserHandler handler = (XMLUserHandler) dh;
					sParser.parse(inSource, handler);

				} else if (dh.getClass() == XMLImportHandler.class) {
					System.out.println("Importing");
					XMLImportHandler handler = (XMLImportHandler) dh;
					sParser.parse(inSource, handler);
				}
			} else {
				System.out.println(file.getPath() + " does not exist");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			// e.printStackTrace();
		}

	}

}
