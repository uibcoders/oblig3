//Class DataPanel the class responsible for created the JTable to present issues.
package no.uib.info233.v2016.sto020_can013.oblig3.gui.panels;

import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.Issue;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueModel;

/**
 * A panel which uses the Custom tableModel to display data from an issueList,
 * and also to update the model with ne data
 * 
 * @author sto020
 * @version 1.0
 *
 */

public class DataPanel extends JPanel {

	private static final long serialVersionUID = -7617058389026678111L;

	private JTable table; // Jtable field to be used to present issues.
	private IssueModel iModel; // IssueModel field variable.

	/**
	 * Constructor of class DataPanel.
	 * 
	 * @param issueList
	 *            the list of issues to be added to iModel field as a
	 *            constructor parameter. instantiates iModel and table from
	 *            their respective class.
	 */
	public DataPanel(List<Issue> issueList) {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		this.iModel = new IssueModel(issueList);

		table = new JTable(iModel); // adds iModel values to table.
		table.setFillsViewportHeight(true);
		table.setShowVerticalLines(false);
		table.setAutoCreateRowSorter(true);
		JScrollPane scrollPane = new JScrollPane(table); // adds the table to
															// the scrollpane.
		add(scrollPane); // adds the scrollpane.

	}

	public void removeAllRows() {
		int rowCount = iModel.getRowCount();
		// Remove rows one by one from the end of the table
		for (int i = rowCount - 1; i >= 0; i--) {
			iModel.removeRow(i);
		}
		iModel.fireTableDataChanged();

	}

	/**
	 * @param newIssues
	 *            the list of new issues to be added to the table. local
	 *            variable of rows in the iModel field stored as an integer
	 *            value. for loop to remove rows one by one from the end of the
	 *            table. for each loop to add new issues to the table.
	 */
	public void update(List<Issue> newIssues) {
		removeAllRows();

		for (Issue issue : newIssues) {
			iModel.addRow(issue);
		}
		iModel.fireTableDataChanged();
	}

	/**
	 * Sets the table to display only one issue
	 * 
	 * @param issue
	 *            The issue to display
	 */
	public void update(Issue issue) {
		removeAllRows();

		iModel.addRow(issue);

		iModel.fireTableDataChanged();
	}

	/**
	 * @return table the value of the table.
	 */
	public JTable getTable() {
		return table;
	}

	/**
	 * @param table
	 *            the table to set to be the new value of field table.
	 */
	public void setTable(JTable table) {
		this.table = table;
	}

	/**
	 * @return the iModel field.
	 */
	public IssueModel getiModel() {
		return iModel;
	}

	/**
	 * @param iModel
	 *            the iModel to set to be the new value of field iModel.
	 */
	public void setiModel(IssueModel iModel) {
		this.iModel = iModel;
	}
}
