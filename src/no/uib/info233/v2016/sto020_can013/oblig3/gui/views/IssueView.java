package no.uib.info233.v2016.sto020_can013.oblig3.gui.views;

import java.awt.Canvas;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import no.uib.info233.v2016.sto020_can013.oblig3.gui.GUI;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.Issue;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.User;
import no.uib.info233.v2016.sto020_can013.oblig3.main.Main;
import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Globals;
import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;

import java.awt.Font;

/**
 * Creates a view to create and edit an Issue, it also updates the main GUI
 * lists
 * 
 * @author Sto020
 * @author can013
 */
public class IssueView extends Canvas {

	private static final long serialVersionUID = 1L;

	private JButton closeButton; // button for closing the issue view
	private JButton saveButton; // button to save information.
	private JFrame frame; // frame for the issue view.
	private Issue issue; // particular issue to be shown.
	private IssueTracker iTracker;

	private JLabel ID; // label for ID.
	private JLabel created; // label for when issue was created.
	private JComboBox<String> userComboBox, priorityComboBox, statusComboBox;
	private JTextField text; // textfield
	private JTextField location; // location textfield.

	private int issueID = 0; // sets field issueID to 0.
	private String username;
	private boolean anonymous = false;
	private GUI gui; // gui instance for be used.
	private JLabel label;

	/**
	 * Constructor for the IssueView class.
	 * 
	 * @param issueID
	 *            the ID of the issue to be shown.
	 * @param username
	 *            The user editing the issue.
	 * 
	 * @param iTracker
	 */
	public IssueView(int issueID, String username, final IssueTracker iTracker, boolean anonymous) {

		this.issueID = issueID;
		this.username = username;
		this.iTracker = iTracker;
		this.anonymous = anonymous;

		frame = new JFrame("Issue");
		this.frame.getContentPane().setLayout(new GridLayout(8, 2));

		JLabel IDlabel = new JLabel("Issue ID:");
		this.ID = new JLabel();
		this.ID.setFont(new Font("Tahoma", Font.BOLD, 15));

		JLabel priorityLabel = new JLabel("Priority:");

		JLabel userLabel = new JLabel("Assigned User:");
		this.userComboBox = new JComboBox<String>();

		JLabel textLabel = new JLabel("Description:");
		this.text = new JTextField();

		JLabel locationLabel = new JLabel("Location:");
		this.location = new JTextField();

		this.closeButton = new JButton("Close"); // instantiates new JButton to
													// be
		// the button to close the
		// issueview.
		this.saveButton = new JButton("Save"); // instantiates new JButton to
												// save
												// new information.

		if (this.anonymous) {
			this.saveButton.setText("Send");
		}

		addActionListeners(); // calls this method so actionlisteners can be
								// used.

		JLabel createdLabel = new JLabel("Created:");
		this.created = new JLabel();

		this.label = new JLabel("Status:");

		this.statusComboBox = new JComboBox<String>(Globals.statusList);

		this.priorityComboBox = new JComboBox<String>(Globals.priorityList); 
		this.priorityComboBox.removeItem("All"); //dirty
		this.priorityComboBox.setFont(new Font("Tahoma", Font.BOLD, 15)); // sets
																			// a
																			// new
		// font
		// to the
		// priority combo
		

		frame.getContentPane().add(IDlabel);
		frame.getContentPane().add(ID);
		frame.getContentPane().add(createdLabel);
		frame.getContentPane().add(created);
		frame.getContentPane().add(label);
		frame.getContentPane().add(this.statusComboBox);
		frame.getContentPane().add(priorityLabel);

		frame.getContentPane().add(this.priorityComboBox);
		if (!anonymous) {
			frame.getContentPane().add(userLabel);
			frame.getContentPane().add(this.userComboBox);
		}
		frame.getContentPane().add(textLabel);
		frame.getContentPane().add(this.text);
		frame.getContentPane().add(locationLabel);
		frame.getContentPane().add(this.location);
		frame.getContentPane().add(this.closeButton);
		frame.getContentPane().add(this.saveButton);

		frame.setSize(400, 200);

		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		if (issueID == -1) {
			this.issue = new Issue();
			this.issue.setID(iTracker.getIssueList().size() + 1);
		} else {
			this.issue = this.iTracker.getIssueByID(issueID);
		}

		setIssue(this.issue);

		if (!anonymous) {
			this.gui = GUI.getInstance();
			frame.setLocationRelativeTo(this.gui);
		}
		frame.setVisible(true);

	}

	/**
	 * @param issue
	 *            the input to set up the information about the issue in
	 *            issueView. sets the information and position to elements in
	 *            issueView. try / catch block to attempt to fill the combobox
	 *            with usernames. catch exception and prints out message to
	 *            screen.
	 */
	private void setIssue(Issue issue) {
		this.ID.setText(Integer.toString(issue.getID()));
		// this.priority.setValue(issue.getPriority());
		this.text.setText(issue.getDescriptiveText());
		this.text.setCaretPosition(0);
		this.location.setText(issue.getLocation());
		this.location.setCaretPosition(0);

		DateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");

		if (issue.getCreatedDate() != null) { // checks that the creation date
												// on this issue is not equal to
												// null.
			this.created.setText(issue.getCreatedDate().toString()); // sets the
																		// text
			// on created
			// JLabel.
		} else {
			// get current date time with Date()

			this.created.setText(dateFormat.format(new Date()));
		}

		if (this.issue.getStatus() != null) {
			this.statusComboBox.setSelectedItem(this.issue.getStatus());

		} else if (this.anonymous) {
			this.statusComboBox.setSelectedItem("Received");
			this.statusComboBox.setEnabled(false);
		} else {
			this.statusComboBox.setSelectedItem("Assigned Employee");

		}

		if (this.issue.getPriority() != null) {
			this.priorityComboBox.setSelectedItem(this.issue.getPriority());

		} else {
			this.priorityComboBox.setSelectedItem("None");
			if (anonymous) {
				this.priorityComboBox.setEnabled(false);
			}

		}
		if (!anonymous) {
			try {

				for (User u : this.iTracker.getSortedUsers()) {
					this.userComboBox.addItem(u.getUsername());

				}
				this.userComboBox.setSelectedItem(issue.getAssignedUser().getUsername());
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}

	}

	/*
	 * void method that takes to parameters. adds actionListener to the close
	 * button. if clicked visibility is set to false and frame is removed.
	 */
	private void addActionListeners() {
		this.closeButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				frame.dispose();
			}
		});

		/*
		 * actionlistener method for the save button. when button is clicked,
		 * information is saved and updated. else JOptionPane to be shown with
		 * an error message.
		 * 
		 */
		this.saveButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				User assigned_user = null;
				LocalDate created_date = Utils.StringToLocalDate(created.getText());
				String status = null;
				String descriptive_text = null;
				String location_set = null;

				// Deletes the old issue and creates a new one with the same id
				if (iTracker.removeIssue(issue) || issueID == -1) {
					// System.out.println("issue Removed");
					String tmpPriority = (String) priorityComboBox.getSelectedItem();
					String createdBy = null;
					status = (String) statusComboBox.getSelectedItem();

					if (issueID == -1 && !anonymous) {
						createdBy = username;
						assigned_user = iTracker.getUser((String) userComboBox.getSelectedItem());

					} else if (anonymous) {
						createdBy = "Anonymous";
						username = null;

					} else {
						assigned_user = iTracker.getUser((String) userComboBox.getSelectedItem());
						createdBy = issue.getCreatedBy();
					}

					if (!text.getText().trim().isEmpty()) {
						descriptive_text = text.getText();
					}

					if (!location.getText().trim().isEmpty()) {
						location_set = location.getText();
					}

					// check if user entered the needed values
					if (descriptive_text == null || location_set == null) {
						String message = "Please enter the following:\r\n";
						if (descriptive_text == null) {
							message += "Description \r\n";
						}
						if (location_set == null) {
							message += "Location\r\n";
						}
						JOptionPane.showMessageDialog(frame, message);
						return;
					}

					int dialogResult = JOptionPane.showConfirmDialog(
							frame, "Are you sure you want to commit this issue?\r\nDescription: "
									+ descriptive_text + "\r\nLocation:     " + location_set,
							"Warning", JOptionPane.YES_NO_OPTION);
					
					if (dialogResult == JOptionPane.YES_OPTION) {
						
						iTracker.addIssue(issue.getID(), tmpPriority, assigned_user, created_date, descriptive_text,
								location_set, status, createdBy, username);

						
						if (!anonymous) {
							Main.updateAllIssueLists();
						}

						Utils.saveData(iTracker, false);
					} else {
						return; // user did not want to save issue
					}
				}
				frame.setVisible(false);
				frame.dispose();

			}
		});
	}
}
