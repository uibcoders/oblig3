package no.uib.info233.v2016.sto020_can013.oblig3.gui.views;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import no.uib.info233.v2016.sto020_can013.oblig3.gui.GUI;
import no.uib.info233.v2016.sto020_can013.oblig3.gui.panels.DataPanel;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.User;
import no.uib.info233.v2016.sto020_can013.oblig3.main.Main;
import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;
import javax.swing.JTabbedPane;
import javax.swing.BoxLayout;

public class UserView extends JFrame {

	private static final long serialVersionUID = -4062320114203994593L;
	private JPanel contentPane; // panel to be used to add elements to.
	private JLabel labelUsername, lblUsername, lblFirstName, lblLastName;  
	private JTextField textFieldUsername; // field so user can enter username.
	private JTextField textFieldFirstname;
	private JTextField textFieldLastname;
	private JButton btnSave;  // button for saving the user.
	private JButton btnClose; // button to close the user view.
	private IssueTracker iTracker;
	private User currentUser;
	private JLabel lblOldPassword;
	private JTextField textFieldOldPassword;
	private JTextField textFieldNewPassword;
	private JLabel lblNewPassword;
	private boolean newUser = false;
	private JTabbedPane tabbedPane;
	private JPanel panel;
	private DataPanel userIssuesPanel = new DataPanel(null);

	/**
	 * Constructor for the UserView, initializes all necessary components and
	 * classes
	 * 
	 * @param user
	 * @param iTracker
	 * @param setup
	 */
	public UserView(User user, IssueTracker iTracker) {
		this.iTracker = iTracker;
		this.currentUser = user;
		if (currentUser.getUsername() == null) {
			newUser = true;
		}

		if (!newUser)
			setTitle("Editing user: " + this.currentUser.getUsername());
		else
			setTitle("Creating new user");

		createComponents();
		addComponents();

		appendListeners();

		init();

	}

	/*
	 * Sets default close ,location and makes the View visible
	 */
	private void init() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 631, 236);

		if (!newUser)
			setUserData();
		setLocationRelativeTo(GUI.getInstance());
		setVisible(true);
	}

	/*
	 * Creates the components of for the UserView
	 */
	private void createComponents() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		if (!newUser) {
			labelUsername = new JLabel();

			labelUsername.setToolTipText("Username\r\n");
		} else {
			textFieldUsername = new JTextField(15);
			textFieldUsername.setToolTipText("Username\r\n");
		}

		lblOldPassword = new JLabel("Old password:");

		textFieldOldPassword = new JPasswordField(15);
	}

	/*
	 * Adds the components created by createComponents to the contentPane
	 */
	private void addComponents() {

		
		
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane);

		panel = new JPanel();
		tabbedPane.addTab("User", null, panel, null);
		panel.setLayout(new GridLayout(0, 2, 0, 0));

		lblUsername = new JLabel("Username:");
		panel.add(lblUsername);
		
		if (!newUser) {
			panel.add(labelUsername);
		}

		if (newUser){
			panel.add(textFieldUsername);
		}

		lblFirstName = new JLabel("First Name:");
		panel.add(lblFirstName);

		textFieldFirstname = new JTextField(15);
		panel.add(textFieldFirstname);
		textFieldFirstname.setToolTipText("First Name");

		lblLastName = new JLabel("Last Name:");
		panel.add(lblLastName);

		textFieldLastname = new JTextField(15);
		panel.add(textFieldLastname);
		textFieldLastname.setToolTipText("Last Name");
		
		if (!newUser && this.currentUser.getLoginPasswordHash() != null) {
			panel.add(lblOldPassword);

			panel.add(textFieldOldPassword);
		}
		
		lblNewPassword = new JLabel("New Password:");
		panel.add(lblNewPassword);

		textFieldNewPassword = new JPasswordField(15);
		panel.add(textFieldNewPassword);

		btnClose = new JButton("Close");
		panel.add(btnClose);
		btnSave = new JButton("Save");
		panel.add(btnSave);

		if (!newUser){
			tabbedPane.addTab("Issues", null, userIssuesPanel, null);
			
			//add a simple mouselistener to the table, so we can edit the issues
			userIssuesPanel.getTable().addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					//Not used
					
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					//Not used
					
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					//Not used
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					//Not used
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					//if user double clicks, then we edit the selected issue
					if (e.getClickCount() == 2) {
						Main.editIssue(false, userIssuesPanel);

					}
					
					
				}
		
			});
		}

	}

	/*
	 * Adds listeners and actions to the buttons
	 */
	private void appendListeners() {
		btnClose.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});

		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveUserData();
			}
		});
	}

	/*
	 * Sets the textfields of the View to the User (currentUser) data
	 */
	private void setUserData() {

		labelUsername.setText(this.currentUser.getUsername());

		textFieldFirstname.setText(this.currentUser.getFirstName());

		textFieldLastname.setText(this.currentUser.getLastName());
		
		
	    userIssuesPanel.update(iTracker.getIssuesByUser(this.currentUser.getUsername()));
		

	}

	/*
	 * Saves the newly created or edited user to the IssueTracker userList
	 */
	private void saveUserData() {

		if (newUser) { // if a new user check if the username exist already
			this.currentUser.setUsername(textFieldUsername.getText().trim());
			if (this.iTracker.checkUserExist(this.currentUser.getUsername())) {
				JOptionPane.showMessageDialog(this,
						String.format("Username '%s' already exist!", this.currentUser.getUsername()));
				return;
			}
		}

		// remove the user from the collection to get ready to add the updated
		// one
		if (this.iTracker.removeUser(currentUser)) {

			this.currentUser.setFirstName(textFieldFirstname.getText());
			this.currentUser.setLastName(textFieldLastname.getText());

			if (!textFieldNewPassword.getText().isEmpty()) {
				if ((textFieldOldPassword.getText().isEmpty() && currentUser.getLoginPasswordHash() == null)
						|| (currentUser.getLoginPasswordHash()
								.equals(Utils.StringHash(textFieldOldPassword.getText(), currentUser.getUsername())))) {
					this.currentUser.setLoginPassword(textFieldNewPassword.getText());
				}

				else {
					this.iTracker.addUser(currentUser);
					JOptionPane.showMessageDialog(this, "Enter the correct old password");
					return;
				}
			} else {
				// transfer password
				this.currentUser.setLoginPasswordHash(currentUser.getLoginPasswordHash());
			}
			this.iTracker.addUser(this.currentUser);
			Utils.saveData(iTracker,false);
			Main.updateUserLists();
		}

		setVisible(false);
		dispose();
	}

	/**
	 * @return the labelUsername
	 */
	public JLabel getLabelUsername() {
		return labelUsername;
	}

	/**
	 * @return the textFieldFirstname
	 */
	public JTextField getTextFieldFirstname() {
		return textFieldFirstname;
	}

	/**
	 * @return the textFieldLastname
	 */
	public JTextField getTextFieldLastname() {
		return textFieldLastname;
	}

	/**
	 * @return the btnSave
	 */
	public JButton getBtnSave() {
		return btnSave;
	}

	/**
	 * @return the btnClose
	 */
	public JButton getBtnClose() {
		return btnClose;
	}

	public DataPanel getUserIssuesPanel() {
		return userIssuesPanel;
	}
}
