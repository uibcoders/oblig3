// Class for the main GUI

package no.uib.info233.v2016.sto020_can013.oblig3.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.List;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import no.uib.info233.v2016.sto020_can013.oblig3.gui.panels.DataPanel;
import no.uib.info233.v2016.sto020_can013.oblig3.gui.panels.DatePicker;
import no.uib.info233.v2016.sto020_can013.oblig3.main.Main;
import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Globals;

/**
 * Creates the main GUI
 * 
 * @author can013
 * @author sto020
 * @version 0.2.0
 */
public class GUI extends JFrame {

	
	private static final long serialVersionUID = -7513854125040781915L;
	private JPanel contentPane; // Panel
	private DataPanel searchDPanel = new DataPanel(null); // the JTable to be in
															// the search tab.
	private DataPanel allIssuesPanel = new DataPanel(null);// the JTable to be
															// in the allIssues
															// tab.
	private DataPanel userNewestIssuesPanel = new DataPanel(null);

	private DataPanel userUnresolvedIssuesPanel = new DataPanel(null);

	private DatePicker dtTo; // field to limit the date search.
	private DatePicker dtFrom; // field to search after issues from creation
								// date and onwards.
	private JComboBox<String> userBox; // dropdown menu for different search
										// options.
	private JComboBox<String> comboBoxPriorities; // Dropdown menu for all priorities on issues.
	private JButton btnUpdate; // updates the search field in the search tab.
	private JTabbedPane tabbedPane;
	private JButton btnNewIssue; // button to add new issues to allIssues tab.
	private List userList;
	private JTextField searchByIDField;
	private JMenuItem menuImportData; // menuitem for importing xml file.
	private JMenuItem menuExitAppliction; // menuitem to exit the application.
	private JMenuItem menuSettingsAddUser; // menuitems to add user.
	private JButton btnGetPriority; // button to retrieve high priority
	private static GUI instance = null;
	private JPanel panel_1;
	private JLabel lblCurrentUser;
	private JPanel panel_2;
	private JPanel panel_3;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Create the frame.
	 * 
	 * @param title
	 *            the title of the GUI frame. Sets up the graphical interface,
	 *            adds relevant items onto it.
	 */
	public GUI(String title) {
		setTitle(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		// Double width = screenSize.getWidth() / 1.8;
		// Double height = screenSize.getHeight() / 1.8;

		// setBounds(100, 100, width.intValue(), height.intValue());
		setBounds(100, 100, 1000, 600);

		// center on user screen, because why not
		setLocation(screenSize.width / 2 - this.getSize().width / 2, screenSize.height / 2 - this.getSize().height / 2);

		createContentPane();

		createHomePanel();

		createSearchPanel();

		createAllIssuesPanel();

		createUsersPanel();

		createMenuBar();

		setVisible(true);
	}

	/**
	 *  method to set up the contentPane.
	 *  Instantiates contentPane, adds restrictions
	 *  and elements to it. 
	 *  adds contentPane to frame.
	 */
	private void createContentPane() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		getContentPane().add(contentPane);
	}

	/**
	 * method for setting up the menu bar.
	 */
	private void createMenuBar() {
		JMenuBar menuBar = new JMenuBar(); // menubar at the top of the frame.
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("File"); // creates a new JMenu and adds it
												// to menuBar.
		menuBar.add(mnNewMenu);
		JMenuItem menuLogout = new JMenuItem("Logout");
		menuLogout.addActionListener(new ActionListener() {  // adds actionlistener to logout item.

			@Override
			public void actionPerformed(ActionEvent e) { // disposes of gui and runs the program again.
				setVisible(false);
				dispose();
				Main.initialize();

			}
		});
		mnNewMenu.add(menuLogout);

		menuImportData = new JMenuItem("Import from XML");
		mnNewMenu.add(menuImportData);

		menuExitAppliction = new JMenuItem("Exit");
		menuExitAppliction.addActionListener(new ActionListener() { // actionlistener to exit.

			@Override
			public void actionPerformed(ActionEvent e) { // Stops the program
				setVisible(false);
				dispose();

			}
		});

		mnNewMenu.add(menuExitAppliction);

		JMenu mnSettings = new JMenu("Users");
		menuBar.add(mnSettings);

		menuSettingsAddUser = new JMenuItem("Add user");
		mnSettings.add(menuSettingsAddUser);
	}

	/**
	 * method to set up user panel.
	 */
	private void createUsersPanel() {

		JScrollPane scrollPane = new JScrollPane();
		tabbedPane.addTab("All Users", null, scrollPane, null);

		userList = new List();
		scrollPane.setViewportView(userList);

		JLabel lblAllUsers = new JLabel("All Users");

		scrollPane.setColumnHeaderView(lblAllUsers);

	}

	/**
	 * method to set up panel for all issues.
	 */
	private void createAllIssuesPanel() {
		JPanel panel = new JPanel();
		tabbedPane.addTab("All Issues", null, panel, null); // tabbedPane for all issues to be viewed.
		SpringLayout sl_panel = new SpringLayout();
		sl_panel.putConstraint(SpringLayout.NORTH, allIssuesPanel, 5, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, allIssuesPanel, 0, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.EAST, allIssuesPanel, -150, SpringLayout.EAST, panel);
		panel.setLayout(sl_panel);

		btnNewIssue = new JButton("New issue"); // button to create new issues.
		sl_panel.putConstraint(SpringLayout.NORTH, btnNewIssue, 10, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, btnNewIssue, 19, SpringLayout.EAST, allIssuesPanel);
		panel.add(btnNewIssue);
		btnNewIssue.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(allIssuesPanel);

	}

	/**
	 * Sets up the search panel.
	 * adds elements to the panel.
	 */
	private void createSearchPanel() {

		Calendar rightNow = Calendar.getInstance();
		JPanel panel_5 = new JPanel();
		tabbedPane.addTab("Search", null, panel_5, null); 
		SpringLayout sl_panel_5 = new SpringLayout();
		panel_5.setLayout(sl_panel_5);

		JLabel lblNewLabel = new JLabel("Show Issues from:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));

		sl_panel_5.putConstraint(SpringLayout.NORTH, lblNewLabel, 10, SpringLayout.NORTH, panel_5);
		sl_panel_5.putConstraint(SpringLayout.WEST, lblNewLabel, 35, SpringLayout.WEST, panel_5);
		sl_panel_5.putConstraint(SpringLayout.EAST, lblNewLabel, -350, SpringLayout.EAST, panel_5);
		panel_5.add(lblNewLabel);

		btnUpdate = new JButton("Show me");
		sl_panel_5.putConstraint(SpringLayout.SOUTH, btnUpdate, -5, SpringLayout.NORTH, searchDPanel);
		btnUpdate.setFont(new Font("Tahoma", Font.BOLD, 12));
		panel_5.add(btnUpdate);
		dtFrom = new DatePicker(rightNow);
		sl_panel_5.putConstraint(SpringLayout.NORTH, dtFrom, 5, SpringLayout.SOUTH, lblNewLabel);
		sl_panel_5.putConstraint(SpringLayout.WEST, dtFrom, 110, SpringLayout.WEST, panel_5);

		panel_5.add(dtFrom);
		JLabel lblTo = new JLabel("to");
		sl_panel_5.putConstraint(SpringLayout.NORTH, lblTo, 28, SpringLayout.SOUTH, lblNewLabel);
		sl_panel_5.putConstraint(SpringLayout.WEST, lblTo, 41, SpringLayout.EAST, dtFrom);
		lblTo.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_5.add(lblTo);

		dtTo = new DatePicker(rightNow);
		sl_panel_5.putConstraint(SpringLayout.WEST, dtTo, 38, SpringLayout.EAST, lblTo);
		sl_panel_5.putConstraint(SpringLayout.SOUTH, dtTo, 0, SpringLayout.SOUTH, dtFrom);

		panel_5.add(dtTo);
		sl_panel_5.putConstraint(SpringLayout.NORTH, searchDPanel, 144, SpringLayout.NORTH, panel_5);
		sl_panel_5.putConstraint(SpringLayout.WEST, searchDPanel, 5, SpringLayout.WEST, panel_5);
		sl_panel_5.putConstraint(SpringLayout.SOUTH, searchDPanel, 0, SpringLayout.SOUTH, panel_5);
		sl_panel_5.putConstraint(SpringLayout.EAST, searchDPanel, -5, SpringLayout.EAST, panel_5);
		panel_5.add(searchDPanel);

		userBox = new JComboBox<String>();
		sl_panel_5.putConstraint(SpringLayout.EAST, userBox, 140, SpringLayout.WEST, panel_5);
		sl_panel_5.putConstraint(SpringLayout.SOUTH, userBox, -8, SpringLayout.NORTH, searchDPanel);
		panel_5.add(userBox);

		searchByIDField = new JTextField(5);
		sl_panel_5.putConstraint(SpringLayout.NORTH, searchByIDField, 0, SpringLayout.NORTH, btnUpdate);
		JLabel idLabelSearch = new JLabel("Search by ID : ");
		sl_panel_5.putConstraint(SpringLayout.WEST, searchByIDField, 6, SpringLayout.EAST, idLabelSearch);
		sl_panel_5.putConstraint(SpringLayout.NORTH, idLabelSearch, 0, SpringLayout.NORTH, btnUpdate);
		idLabelSearch.setFont(new Font("Tahoma", Font.BOLD, 12));
		panel_5.add(searchByIDField);
		panel_5.add(idLabelSearch);

		JLabel lblFilterBy = new JLabel("Filter by:");
		sl_panel_5.putConstraint(SpringLayout.SOUTH, lblFilterBy, -15, SpringLayout.NORTH, searchDPanel);
		sl_panel_5.putConstraint(SpringLayout.WEST, userBox, 6, SpringLayout.EAST, lblFilterBy);
		sl_panel_5.putConstraint(SpringLayout.WEST, lblFilterBy, 0, SpringLayout.WEST, searchDPanel);
		panel_5.add(lblFilterBy);

		JRadioButton radioAll = new JRadioButton("All");
		sl_panel_5.putConstraint(SpringLayout.NORTH, radioAll, 0, SpringLayout.NORTH, btnUpdate);
		sl_panel_5.putConstraint(SpringLayout.EAST, radioAll, 0, SpringLayout.EAST, lblTo);
		radioAll.setFont(new Font("Tahoma", Font.PLAIN, 10));
		sl_panel_5.putConstraint(SpringLayout.EAST, btnUpdate, -15, SpringLayout.WEST, radioAll);
		radioAll.setBackground(Color.LIGHT_GRAY);
		sl_panel_5.putConstraint(SpringLayout.WEST, radioAll, 280, SpringLayout.WEST, panel_5);
		radioAll.setSelected(true);
		radioAll.setActionCommand("all");
		buttonGroup.add(radioAll);
		panel_5.add(radioAll);

		JRadioButton radioUnresolved = new JRadioButton("Unresolved");
		sl_panel_5.putConstraint(SpringLayout.WEST, radioUnresolved, 0, SpringLayout.EAST, radioAll);
		radioUnresolved.setFont(new Font("Tahoma", Font.PLAIN, 10));
		radioUnresolved.setBackground(Color.LIGHT_GRAY);
		sl_panel_5.putConstraint(SpringLayout.NORTH, radioUnresolved, 0, SpringLayout.NORTH, btnUpdate);
		radioUnresolved.setActionCommand("unresolved");

		buttonGroup.add(radioUnresolved);
		panel_5.add(radioUnresolved);

		JRadioButton radioResolved = new JRadioButton("Resolved");
		sl_panel_5.putConstraint(SpringLayout.WEST, radioResolved, 90, SpringLayout.WEST, radioUnresolved);
		radioResolved.setFont(new Font("Tahoma", Font.PLAIN, 10));
		sl_panel_5.putConstraint(SpringLayout.EAST, radioUnresolved, 0, SpringLayout.WEST, radioResolved);
		radioResolved.setBackground(Color.LIGHT_GRAY);
		sl_panel_5.putConstraint(SpringLayout.NORTH, radioResolved, 0, SpringLayout.NORTH, btnUpdate);
		radioResolved.setActionCommand("resolved");

		buttonGroup.add(radioResolved);
		panel_5.add(radioResolved);

		JPanel panel_4 = new JPanel();
		sl_panel_5.putConstraint(SpringLayout.EAST, searchByIDField, -20, SpringLayout.WEST, panel_4);
		sl_panel_5.putConstraint(SpringLayout.EAST, idLabelSearch, -76, SpringLayout.WEST, panel_4);
		sl_panel_5.putConstraint(SpringLayout.NORTH, panel_4, 0, SpringLayout.NORTH, dtFrom);
		sl_panel_5.putConstraint(SpringLayout.WEST, panel_4, -205, SpringLayout.EAST, panel_5);
		sl_panel_5.putConstraint(SpringLayout.SOUTH, panel_4, -17, SpringLayout.NORTH, searchDPanel);
		sl_panel_5.putConstraint(SpringLayout.EAST, panel_4, 0, SpringLayout.EAST, searchDPanel);
		panel_4.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		panel_5.add(panel_4);
		SpringLayout sl_panel_4 = new SpringLayout();
		panel_4.setLayout(sl_panel_4);
		
		comboBoxPriorities = new JComboBox<String>(Globals.priorityList);
		sl_panel_4.putConstraint(SpringLayout.NORTH, comboBoxPriorities, 10, SpringLayout.NORTH, panel_4);
		sl_panel_4.putConstraint(SpringLayout.WEST, comboBoxPriorities, 10, SpringLayout.WEST, panel_4);
		sl_panel_4.putConstraint(SpringLayout.EAST, comboBoxPriorities, -10, SpringLayout.EAST, panel_4);
		sl_panel_5.putConstraint(SpringLayout.WEST, comboBoxPriorities, 48, SpringLayout.WEST, panel_4);
		sl_panel_5.putConstraint(SpringLayout.SOUTH, comboBoxPriorities, -63, SpringLayout.NORTH, lblFilterBy);
		sl_panel_5.putConstraint(SpringLayout.EAST, comboBoxPriorities, 44, SpringLayout.WEST, lblNewLabel);
		panel_4.add(comboBoxPriorities);

		btnGetPriority = new JButton("Get");
		sl_panel_4.putConstraint(SpringLayout.NORTH, btnGetPriority, 10, SpringLayout.SOUTH, comboBoxPriorities);
		sl_panel_4.putConstraint(SpringLayout.WEST, btnGetPriority, 10, SpringLayout.WEST, panel_4);
		sl_panel_4.putConstraint(SpringLayout.EAST, btnGetPriority, -10, SpringLayout.EAST, panel_4);
		sl_panel_5.putConstraint(SpringLayout.NORTH, btnGetPriority, 0, SpringLayout.NORTH, panel_4);
		sl_panel_5.putConstraint(SpringLayout.SOUTH, btnGetPriority, -71, SpringLayout.SOUTH, panel_4);
		sl_panel_5.putConstraint(SpringLayout.NORTH, comboBoxPriorities, 7, SpringLayout.SOUTH, btnGetPriority);
		sl_panel_5.putConstraint(SpringLayout.WEST, btnGetPriority, 10, SpringLayout.WEST, panel_4);
		sl_panel_5.putConstraint(SpringLayout.EAST, btnGetPriority, -756, SpringLayout.EAST, panel_5);
		panel_4.add(btnGetPriority);

		JLabel lblIssuesByPriority = new JLabel("Issues by Priority");
		sl_panel_5.putConstraint(SpringLayout.NORTH, lblIssuesByPriority, 1, SpringLayout.NORTH, lblNewLabel);
		sl_panel_5.putConstraint(SpringLayout.EAST, lblIssuesByPriority, -102, SpringLayout.EAST, panel_5);
		panel_5.add(lblIssuesByPriority);
		
		JRadioButton radioUnchanged = new JRadioButton("Unchanged");
		sl_panel_5.putConstraint(SpringLayout.WEST, radioUnchanged, 90, SpringLayout.WEST, radioResolved);
		radioUnchanged.setFont(new Font("Tahoma", Font.PLAIN, 10));
		radioUnchanged.setBackground(Color.LIGHT_GRAY);
		sl_panel_5.putConstraint(SpringLayout.EAST, radioResolved, 0, SpringLayout.WEST, radioUnchanged);
		sl_panel_5.putConstraint(SpringLayout.NORTH, radioUnchanged, 0, SpringLayout.NORTH, btnUpdate);
		radioUnchanged.setActionCommand("unchanged");
		buttonGroup.add(radioUnchanged);
		panel_5.add(radioUnchanged);

	}

	/**
	 * method to set up the home panel
	 * the panel the user sees first when logging in.
	 * adds elemements to it.
	 */
	private void createHomePanel() {
		panel_1 = new JPanel();
		tabbedPane.addTab("Home", null, panel_1, null);
		SpringLayout sl_panel_1 = new SpringLayout();
		panel_1.setLayout(sl_panel_1);

		JLabel lblUnresolvedIssues = new JLabel("Unresolved Issues");
		sl_panel_1.putConstraint(SpringLayout.SOUTH, lblUnresolvedIssues, -250, SpringLayout.SOUTH, panel_1);
		lblUnresolvedIssues.setFont(new Font("Tahoma", Font.BOLD, 12));
		sl_panel_1.putConstraint(SpringLayout.WEST, lblUnresolvedIssues, 403, SpringLayout.WEST, panel_1);
		sl_panel_1.putConstraint(SpringLayout.EAST, lblUnresolvedIssues, 516, SpringLayout.WEST, panel_1);
		panel_1.add(lblUnresolvedIssues);

		JLabel lblNewestIssues = new JLabel("Newest Issues");
		sl_panel_1.putConstraint(SpringLayout.SOUTH, lblNewestIssues, -200, SpringLayout.NORTH, lblUnresolvedIssues);
		lblNewestIssues.setFont(new Font("Tahoma", Font.BOLD, 12));
		sl_panel_1.putConstraint(SpringLayout.WEST, lblNewestIssues, 403, SpringLayout.WEST, panel_1);
		panel_1.add(lblNewestIssues);

		lblCurrentUser = new JLabel();
		sl_panel_1.putConstraint(SpringLayout.SOUTH, lblCurrentUser, -384, SpringLayout.SOUTH, panel_1);
		lblCurrentUser.setFont(new Font("Tahoma", Font.BOLD, 14));
		sl_panel_1.putConstraint(SpringLayout.EAST, lblCurrentUser, 385, SpringLayout.WEST, panel_1);
		lblCurrentUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurrentUser.setBackground(Color.WHITE);
		sl_panel_1.putConstraint(SpringLayout.WEST, lblCurrentUser, 10, SpringLayout.WEST, panel_1);
		panel_1.add(lblCurrentUser);

		panel_2 = new JPanel();
		sl_panel_1.putConstraint(SpringLayout.NORTH, panel_2, 6, SpringLayout.SOUTH, lblNewestIssues);
		sl_panel_1.putConstraint(SpringLayout.SOUTH, panel_2, -6, SpringLayout.NORTH, lblUnresolvedIssues);
		sl_panel_1.putConstraint(SpringLayout.WEST, panel_2, 0, SpringLayout.WEST, lblUnresolvedIssues);
		sl_panel_1.putConstraint(SpringLayout.EAST, panel_2, -10, SpringLayout.EAST, panel_1);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.X_AXIS));
		panel_2.add(userNewestIssuesPanel);
		panel_1.add(panel_2);

		panel_3 = new JPanel();
		sl_panel_1.putConstraint(SpringLayout.NORTH, panel_3, 6, SpringLayout.SOUTH, lblUnresolvedIssues);
		sl_panel_1.putConstraint(SpringLayout.WEST, panel_3, 403, SpringLayout.WEST, panel_1);
		sl_panel_1.putConstraint(SpringLayout.SOUTH, panel_3, 200, SpringLayout.SOUTH, lblUnresolvedIssues);
		sl_panel_1.putConstraint(SpringLayout.EAST, panel_3, -10, SpringLayout.EAST, panel_1);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.X_AXIS));
		panel_3.add(userUnresolvedIssuesPanel);
		panel_1.add(panel_3);

		JLabel lblCurrentUser_1 = new JLabel("Current User");
		sl_panel_1.putConstraint(SpringLayout.NORTH, lblCurrentUser, 43, SpringLayout.SOUTH, lblCurrentUser_1);
		lblCurrentUser_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		sl_panel_1.putConstraint(SpringLayout.NORTH, lblCurrentUser_1, 0, SpringLayout.NORTH, lblNewestIssues);
		sl_panel_1.putConstraint(SpringLayout.WEST, lblCurrentUser_1, 0, SpringLayout.WEST, lblCurrentUser);
		panel_1.add(lblCurrentUser_1);
	}
	/**
	 * Creates an instance of a GUI.
	 * If one already exist, it will return the existing instance
	 */
	public static synchronized GUI getInstance() {
		if (instance == null) {
			instance = new GUI("");
		}
		return instance;
	}

	/**
	 * Resets the GUI instance to null so a new one can be created by getInstance
	 */
	public static synchronized void setInstance() {
		instance = null;
	}

	/**
	 * @return the searchDpanel an instance of the DataPanel class.
	 */

	public DataPanel getsearchDPanel() {
		return searchDPanel;
	}

	/**
	 * @return allIssuesPanel an instance of the DataPanel class.
	 */
	public DataPanel getAllIssuesPanel() {
		return allIssuesPanel;
	}

	/**
	 * @return the userNewestIssuesPanel
	 */
	public DataPanel getUserNewestIssuesPanel() {
		return userNewestIssuesPanel;
	}

	/**
	 * @return the userUnresolvedIssuesPanel
	 */
	public DataPanel getUserUnresolvedIssuesPanel() {
		return userUnresolvedIssuesPanel;
	}

	/**
	 * @return dtFrom an instance of the DatePicker class.
	 */
	public DatePicker getDtFrom() {
		return dtFrom;
	}

	/**
	 * @param dtFrom
	 *            instance of DatePicker to be the new value of field dtFrom.
	 */
	public void setDtFrom(DatePicker dtFrom) {
		this.dtFrom = dtFrom;
	}

	/**
	 * @return dtTo the instance of the DatePicker class.
	 */
	public DatePicker getDtTo() {
		return dtTo;
	}

	/**
	 * @param dtTo
	 *            instance of the DatePicker class to be the new value of field
	 *            dtTo.
	 */
	public void setDtTo(DatePicker dtTo) {
		this.dtTo = dtTo;
	}

	/**
	 * @return the btnUpdate button.
	 */
	public JButton getBtnUpdate() {
		return btnUpdate;
	}

	/**
	 * @return the value of the value of the text field.
	 */
	public JTextField getSearchByIDField() {
		return this.searchByIDField;
	}

	/**
	 * @param btnUpdate
	 *            to be the new value of btnUpdate field.
	 */
	public void setBtnUpdate(JButton btnUpdate) {
		this.btnUpdate = btnUpdate;
	}

	/**
	 * @return the userBox of stored users.
	 */
	public JComboBox<String> getUserBox() {
		return userBox;
	}

	/**
	 * @return the comboBoxPriorities of all issue priorities.
	 */
	public JComboBox<String> getComboBoxPriorities() {
		return comboBoxPriorities;
	}

	/**
	 * @return userList the list of users.
	 */
	public List getUserList() {
		return userList;
	}

	/**
	 * @return the tabbedPane.
	 */
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	/**
	 * @return the menuImportData
	 */
	public JMenuItem getMenuImportData() {
		return menuImportData;
	}

	/**
	 * @return the btnNewIssue button.
	 */
	public JButton getBtnNewIssue() {
		return btnNewIssue;
	}

	/**
	 * @return the menuExitAppliction
	 */
	public JMenuItem getMenuExitAppliction() {
		return menuExitAppliction;
	}

	/**
	 * @return the menuSettingsAddUser
	 */
	public JMenuItem getMenuSettingsAddUser() {
		return menuSettingsAddUser;
	}

	/**
	 * @return the btnGetPriority button.
	 */
	public JButton getBtnGetPriority() {
		return btnGetPriority;
	}

	/**
	 * @return the lblCurrentUser
	 */
	public JLabel getLblCurrentUser() {
		return lblCurrentUser;
	}

	/**
	 * @return the buttonGroup
	 */
	public ButtonGroup getButtonGroup() {
		return buttonGroup;
	}
}
