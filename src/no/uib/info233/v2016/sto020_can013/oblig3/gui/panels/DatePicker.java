package no.uib.info233.v2016.sto020_can013.oblig3.gui.panels;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
 
/**
 * Adapted from http://www.coderanch.com/t/660235/Wiki/Date-Picker-Dialog -  to fit application by sto020
 * 
 * Creates a Panel for choosing the day, month and year
 *
 * @author sto020
 * @version 1.1.0
 */
 
public class DatePicker extends JPanel implements ActionListener {
  
	private static final long serialVersionUID = -5274106813665006958L;
	private String[] monthNames = {"Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec"};
    private int[] daysPerMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
    private Calendar dateAndTimeIn = Calendar.getInstance();
    private Calendar dateAndTimeOut = Calendar.getInstance();
    private int year = 2016;
    private int month = 0;
    private int day = 1;
    private String monthName = null;
    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
 

    private JLabel lblDate;
    private JSpinner spnDay;
    private JSpinner spnMonth;
    private JSpinner spnYear;
 
    /**
     * Retrieves the results from dialog
     *
     * @return dateAndTimeOut Date as modified by the dialog box
     * Note:                  Time portion of Calendar is not modified
     */
    public Calendar getAnswer() { return dateAndTimeOut; }
 
    /** 
     * 
     * Creates new form DatePicker
     *
     * @param frame Frame that the dialog is being called from
     * @param modal
     * @param dateAndTime Date used to initialize the dialog
     */
    public DatePicker (Calendar dateAndTime) {
        initComponents();
 
        // Save input in case user clicks Cancel or Close
        year = dateAndTime.get(Calendar.YEAR);
        month = dateAndTime.get(Calendar.MONTH);
        day = dateAndTime.get(Calendar.DAY_OF_MONTH);
        dateAndTimeIn.set(year, month, day);
 
        // Load output with inital value
        dateAndTimeOut.setTime(dateAndTime.getTime());
 
        // Set the initial values for the year, month and day spinners
        spnYear.setValue(year);
        spnMonth.setValue(monthNames[month]);
        spnDay.setValue(day);
 
        // Display the initial value at the top of the dialog
        lblDate.setText(sdf.format(dateAndTime.getTime()));
      setVisible(true);
    }
 
    /** This method is called from within the constructor to initialize the form. */
    private void initComponents() {
        GridBagConstraints gridBagConstraints;
 
        lblDate = new JLabel();
       
        spnMonth = new JSpinner();
        spnDay = new JSpinner();
        spnYear = new JSpinner();
 
        setLayout(new GridBagLayout());
 
        lblDate.setText("Date:");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
      //  gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        add(lblDate, gridBagConstraints);
 
      
     
 
        spnMonth.setModel(new SpinnerListModel(monthNames));
        spnMonth.addChangeListener(new ChangeListener() {
            public void stateChanged (ChangeEvent evt) {
                spnMonthStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 25;
       // gridBagConstraints.insets = new Insets(2, 2, 2, 2);
       add(spnMonth, gridBagConstraints);
 
        spnDay.setModel(new SpinnerNumberModel(1, 1, 31, 1));
        spnDay.setEditor(new JSpinner.NumberEditor(spnDay, "##"));
        spnDay.addChangeListener(new ChangeListener() {
            public void stateChanged (ChangeEvent evt) {
                spnDayStateChanged(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        //gridBagConstraints.insets = new Insets(2, 2, 2, 2);
        add(spnDay, gridBagConstraints);
 
        spnYear.setModel(new SpinnerNumberModel(2010, 2000, 2050, 1));
        spnYear.setEditor(new JSpinner.NumberEditor(spnYear, "####"));
        spnYear.addChangeListener(new ChangeListener() {
            public void stateChanged (ChangeEvent evt) {
                spnYearStateChanged(evt);
            }
        });
        
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
     //   gridBagConstraints.insets = new Insets(2, 2, 2, 2);
       add(spnYear, gridBagConstraints);
 
    }
 

 
    /**
     * Event handler for changes to the month spinner
     * Fixes last day of month
     * 
     * @param evt not used
     */
    private void spnMonthStateChanged (ChangeEvent evt) {
        SpinnerModel listModel = spnMonth.getModel();
        if (listModel instanceof SpinnerListModel) {
            monthName = ((SpinnerListModel)listModel).getValue().toString();
            for(int i=0; i<monthNames.length; i++) {
                if (monthName.equals(monthNames[i])) {
                    month = i;
                }
            }
            // Change day if current display is past the end of the new month
            if (day > daysPerMonth[month]) {
                day = daysPerMonth[month];
                spnDay.setValue(day);
                dateAndTimeOut.set(Calendar.DAY_OF_MONTH, day);
            }
            dateAndTimeOut.set(Calendar.MONTH, month);
            lblDate.setText(sdf.format(dateAndTimeOut.getTime()));
        }
    }
 
    /**
     * Event handler for changes to the day spinner
     * Limits day to the last day of month
     *
     * @param evt Not used
     */
    private void spnDayStateChanged (ChangeEvent evt) {
        SpinnerModel numberModel = spnDay.getModel();
        if (numberModel instanceof SpinnerNumberModel) {
            day = (Integer) ((SpinnerNumberModel)numberModel).getValue();
            // Limit day to end of month
            if (isLeapYear(year)) {
                daysPerMonth[1] = 29;
            } else{
                daysPerMonth[1] = 28;
            }
            if (day > daysPerMonth[month]) {
                day = daysPerMonth[month];
                spnDay.setValue(day);
            }
            dateAndTimeOut.set(Calendar.DAY_OF_MONTH, day);
            lblDate.setText(sdf.format(dateAndTimeOut.getTime()));
        }
    }
 
    /**
     * Event handler for changes to year spinner
     * 
     * @param evt Not used
     */
    private void spnYearStateChanged (ChangeEvent evt) {
        SpinnerModel numberModel1 = spnYear.getModel();
        if (numberModel1 instanceof SpinnerNumberModel) {
            year = (Integer) ((SpinnerNumberModel)numberModel1).getValue();
            dateAndTimeOut.set(Calendar.YEAR, year);
            lblDate.setText(sdf.format(dateAndTimeOut.getTime()));
        }
    }
 
    /**
     * Determines if year is a leap year
     * 
     * @param year
     * @return true if year is a leap year
     */
    private boolean isLeapYear (int year) {
        boolean leapYear = false;
        if (year % 400 == 0){
            leapYear = true;
        } else if (year % 100 == 0){
            leapYear = false;
        } else if (year % 4 == 0){
            leapYear = true;
        }
       return leapYear;
    }
 
    public void actionPerformed (ActionEvent ae) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

	public String getDateAndTimeOut() {
		return sdf.format(dateAndTimeOut.getTime());
	}
}