//Class LoginScreen the graphical userinterface to log into the program.

package no.uib.info233.v2016.sto020_can013.oblig3.gui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 * A loginscreen class, needs Username and Password of a saved user
 * 
 * @author can013.
 * @author sto020.
 */
public class LoginScreen extends JFrame {

	private static final long serialVersionUID = -676386693347969269L;
	JButton blogin,btnAddIssue;// login button.
	JPanel loginpanel; // login panel.
	JTextField txuser; // textfield for username input.
	JTextField pass; // textfield to password input.
	JLabel username; // username lable.
	JLabel password; // password label.

	/**
	 * Constructor for the class LoginScreen. sets up the login screen.
	 * instantiates field variables. sets bounds to screen elements. adds
	 * actionlisteners to login button and username textfield. adds fields to
	 * the loginpanel.
	 */
	public LoginScreen() {
		super("Login");
		super.setResizable(false);
		setSize(300, 150);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		// center on user screen, because why not
		setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);

		createComponents();

		appendListeners();

		init();

	}

	/*
	 * method that creates the components of the frame sets their position
	 */
	private void createComponents() {
		blogin = new JButton("Login");
		loginpanel = new JPanel();
		txuser = new JTextField(15);
		pass = new JPasswordField(15);
		username = new JLabel("Username: ");
		password = new JLabel("Password: ");
		btnAddIssue = new JButton("Add Issue");
		loginpanel.setLayout(null);

		txuser.setBounds(100, 20, 150, 20);
		pass.setBounds(100, 50, 150, 20);
		blogin.setBounds(100, 80, 80, 20);
		username.setBounds(20, 18, 80, 20);
		password.setBounds(20, 53, 80, 20);
		btnAddIssue.setBounds(205, 90, 89, 25);

	}

	/*
	 * method to add actionlisteners to username input field and password field.
	 */
	private void appendListeners() {
		txuser.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				pass.requestFocus();
			}
		});

		pass.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				blogin.doClick();
			}
		});
	}

	/*
	 * Adds the components to the login panel and sets it to visible.
	 */
	private void init() {
		loginpanel.add(blogin);
		loginpanel.add(txuser);
		loginpanel.add(pass);
		loginpanel.add(username);
		loginpanel.add(password);

		getContentPane().add(loginpanel);
		
		
		loginpanel.add(btnAddIssue);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	/**
	 * @return blogin the login button.
	 */
	public JButton getBlogin() {
		return blogin;
	}

	/**
	 * @return the btnAddIssue
	 */
	public JButton getBtnAddIssue() {
		return btnAddIssue;
	}

	/**
	 * @param blogin
	 *            to set the new value of the login button.
	 */
	public void setBlogin(JButton blogin) {
		this.blogin = blogin;
	}

	/**
	 * @return txuser the username textfield.
	 */
	public JTextField getTxuser() {
		return txuser;
	}

	/**
	 * @param txuser
	 *            to set the new value of username textfield.
	 */
	public void setTxuser(JTextField txuser) {
		this.txuser = txuser;
	}

	/**
	 * @return pass the password textfield.
	 */
	public JTextField getPass() {
		return pass;
	}

	/**
	 * @param pass
	 *            to set the new value of password textfield.
	 */
	public void setPass(JTextField pass) {
		this.pass = pass;
	}
}
