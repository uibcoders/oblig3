// Class to only report issues without requiring login.

package no.uib.info233.v2016.sto020_can013.oblig3.main;


import no.uib.info233.v2016.sto020_can013.oblig3.gui.views.IssueView;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;

/**
 * @author sto020
 *
 */
public class ReportIssue {

	private static IssueTracker iTracker = null;

	/*
	 * Main method to start the program.
	 * calls the initialize method.
	 */
	public static void main(String[] args) {

		initialize();
	}

	/*
	 * loads data from iTracker using Utils
	 * creates a new IssueView for user to create issues.
	 */
	public static void initialize() {

		iTracker = Utils.loadData(); 

		new IssueView(-1, "Anonymous", iTracker, true);

	}
}
