package no.uib.info233.v2016.sto020_can013.oblig3.main;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.time.LocalDate;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import no.uib.info233.v2016.sto020_can013.oblig3.XML.XMLImportHandler;
import no.uib.info233.v2016.sto020_can013.oblig3.XML.XMLReader;
import no.uib.info233.v2016.sto020_can013.oblig3.events.guiChangeEvent;
import no.uib.info233.v2016.sto020_can013.oblig3.events.guiEvent;
import no.uib.info233.v2016.sto020_can013.oblig3.events.guiMouseEvent;
import no.uib.info233.v2016.sto020_can013.oblig3.gui.GUI;
import no.uib.info233.v2016.sto020_can013.oblig3.gui.LoginScreen;
import no.uib.info233.v2016.sto020_can013.oblig3.gui.panels.DataPanel;
import no.uib.info233.v2016.sto020_can013.oblig3.gui.views.IssueView;
import no.uib.info233.v2016.sto020_can013.oblig3.gui.views.UserView;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.User;
import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;

/**
 * The main class, contains most actionlisteners and methods to be run by the
 * program
 * 
 * @author sto020
 * @author can013
 *
 */

public class Main {

	private static IssueTracker iTracker; // IssueTracker instance as field.
	private static GUI gui; // GUI instance as field.
	private static boolean inPriority = false;
	private static LoginScreen login;

	// The string to add to gui title.
	public static String guiTitle = null;
	private static String currentUser = null;

	/*
	 * Main method to start the program.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				initialize();
			}

		});
	}

	/*
	 * Initializes the program Adds the admin account Creates a Login Screen
	 */
	public static void initialize() {
		GUI.setInstance();
		iTracker = Utils.loadData(); // either creates a new
										// IssueTracker or loads the
										// saved one
		if (iTracker.getUser("admin") == null) {
			User user = new User();
			user.setUsername("admin");
			user.setFirstName("Admin");
			user.setLastName("The Administrator");
			user.setLoginPassword("hello");
			iTracker.addUser(user);

			Utils.saveData(iTracker, false);
		}

		login = new LoginScreen();

		login.getBlogin().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loginAttempt(); // calls this method to check if successful.

			}
		});
		login.getBtnAddIssue().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new IssueView(-1, "Anonymous", iTracker, true);

			}
		});
	}

	/**
	 * @param user
	 *            the instantiated object of User. sets the retrieved fields of
	 *            user to be new local variable userLoggedIn.
	 */
	public static void setUserLoggedIn(User user) {
		currentUser = user.getUsername();
		guiTitle = user.getFirstName() + " " + user.getLastName() + " | " + user.getUsername();
	}

	/*
	 * Executes a login attempt from the LoginScreen Checks if username exist,
	 * if true check if password matches, else FAILED
	 */
	private static void loginAttempt() {

		String username = login.getTxuser().getText();
		if (!username.isEmpty() && iTracker.checkUserExist(username)) {
			User tmpUser = null;
			tmpUser = iTracker.getUser(username);
			if (Utils.StringHash(login.getPass().getText().trim(), login.getTxuser().getText().trim())
					.equals(tmpUser.getLoginPasswordHash())) {
				// JOptionPane.showMessageDialog(null,STATE.validUser;
				setUserLoggedIn(tmpUser);

				validUserState(); // if both if statements are true
									// the program state calls this
									// method and creates new gui +
									// actionlisteners and updates.
				login.dispose(); // removes the login screen.
			} else {
				loginFailed(); // calls the unsuccessful login if password is
								// wrong

			}
		} else {
			loginFailed(); // calls the unsuccessful method if the
							// user does not exist.
		}
	}

	/*
	 * Called if login fails, shows a dialog explaining that you did something
	 * wrong
	 */
	private static void loginFailed() {
		JOptionPane.showMessageDialog(login, "Your username/password was incorrect - please try again!");

	}

	/*
	 * void method that takes no parameters. instantiates gui of the GUI class.
	 * updates the user list and adds all actionlisteners.
	 */
	public static void validUserState() {
		gui = GUI.getInstance();
		gui.setTitle("Welcome " + guiTitle);
		gui.getLblCurrentUser().setText(guiTitle);

		updateUserLists();
		updateAllIssueLists();
		addActionListeners();

	}

	/*
	 * Sets the search panel data of high priority issues
	 */
	public static void searchPriority() {
		inPriority = true;
		if (gui.getComboBoxPriorities().getSelectedItem() != "All") {
			gui.getsearchDPanel()
					.update(iTracker.getPriorityIssues(gui.getComboBoxPriorities().getSelectedItem().toString()));
		} else {
			gui.getsearchDPanel().update(iTracker.getIssueList());
		}
	}

	/*
	 * Simply updates all panels related to issues to the new data if any
	 */
	public static void updateAllIssueLists() {
		gui.getAllIssuesPanel().update(iTracker.getIssueList());
		updateCurrentUserIssues();
		if (!inPriority)
			updateSearchList();
		else
			searchPriority();

	}

	/*
	 * Updates the search panel to match the search criteria, fetches data from
	 * the IssueTracker
	 */
	public static void updateSearchList() {
		inPriority = false;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					LocalDate minDate = Utils.StringToLocalDate(gui.getDtFrom().getDateAndTimeOut());
					LocalDate maxDate = Utils.StringToLocalDate(gui.getDtTo().getDateAndTimeOut());
					String selectedUser = gui.getUserBox().getSelectedItem().toString();
					if (!selectedUser.equals("All Users")) {
						gui.getsearchDPanel().update(iTracker.getIssuesByUserAndDate(selectedUser, minDate, maxDate));

						if (gui.getButtonGroup().getSelection().getActionCommand().equals("all")) { // get
							// all
							gui.getsearchDPanel()
									.update(iTracker.getIssuesByUserAndDate(selectedUser, minDate, maxDate));
						} else if (gui.getButtonGroup().getSelection().getActionCommand().equals("unresolved")) { // get
							// unresolved
							gui.getsearchDPanel()
									.update(iTracker.getIssuesByUserAndDate(selectedUser, minDate, maxDate, false));

						} else if (gui.getButtonGroup().getSelection().getActionCommand().equals("resolved")) {// get
							// resolved
							gui.getsearchDPanel()
									.update(iTracker.getIssuesByUserAndDate(selectedUser, minDate, maxDate, true));

						}
					} else if (gui.getButtonGroup().getSelection().getActionCommand().equals("unchanged")) {// get
						// unchanged
						gui.getsearchDPanel().update(iTracker.getIssueListByStatus("Received"));

					} else {
						if (gui.getButtonGroup().getSelection().getActionCommand().equals("all")) { // get
																									// all
							gui.getsearchDPanel().update(iTracker.getIssuesBetweenDates(minDate, maxDate));

						} else if (gui.getButtonGroup().getSelection().getActionCommand().equals("unresolved")) { // get
																													// unresolved
							gui.getsearchDPanel().update(iTracker.getIssuesBetweenDates(minDate, maxDate, false));

						} else if (gui.getButtonGroup().getSelection().getActionCommand().equals("resolved")) {// get
																												// resolved
							gui.getsearchDPanel().update(iTracker.getIssuesBetweenDates(minDate, maxDate, true));

						} else if (gui.getButtonGroup().getSelection().getActionCommand().equals("unchanged")) {// get
							// unchanged
							gui.getsearchDPanel().update(iTracker.getIssueListByStatus("Received"));

						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	 * void method that takes no parameters. removes userlist and userbox
	 * elements - adds title to userbox. for each loop to input new values in
	 * userList and userBox.
	 */
	public static void updateUserLists() {
		gui.getUserList().removeAll();
		gui.getUserBox().removeAll();
		gui.getUserBox().addItem("All Users");
		for (User user : iTracker.getSortedUsers()) {
			gui.getUserBox().addItem(user.getUsername());
			gui.getUserList().add(user.getUsername());
		}

	}

	/**
	 * Opens a FileDialog and the user chooses the XMLfile to be imported into
	 * the program. clears iTracker issue list and creates a new XMLReader.
	 * XMLReader reads the file, saves data and updates the userlists.
	 */
	public static void importFromXML() {
		// Handles the open button action.

		// Create a file chooser and filter for XML files, sets directory to
		// the one the application is stored in
		FileNameExtensionFilter filter = new FileNameExtensionFilter("XML files", "xml");

		final JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("."));
		fc.setAcceptAllFileFilterUsed(false);
		fc.setFileFilter(filter);

		int returnVal = fc.showOpenDialog(gui);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();

			// Importing the selected file if user agrees
			if (JOptionPane.showConfirmDialog(gui,
					"Importing this file will delete all current saved issues, are you sure?", "Warning",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

				iTracker.clearIssueList();
				XMLReader.readXML(file, new XMLImportHandler(iTracker));
				Utils.saveData(iTracker, false);
				updateUserLists();
				updateAllIssueLists();
				gui.getTabbedPane().setSelectedIndex(2); // switches to all
															// issues tab

			}
		}
	}

	/*
	 * Creates either a new user view or an edit user based on input
	 * 
	 * @param newUser True if adding a new user, false if editing one
	 */
	public static void editUser(boolean newUser) {
		if (!newUser && (gui.getUserList().getSelectedIndex() > -1)) {
			new UserView(iTracker.getUser(gui.getUserList().getSelectedItem()), iTracker);
		} else {
			new UserView(new User(), iTracker);

		}

	}

	/*
	 * Creates or Edits an existing issue.
	 * 
	 * @param newIssue True if creating a new Issue
	 * 
	 * @param panel A DataPanel to edit an issue index from
	 */
	public static void editIssue(boolean newIssue, DataPanel panel) {
		int r = panel.getTable().getSelectedRow();

		if (r >= 0 && r < gui.getsearchDPanel().getTable().getRowCount()) {
			panel.getTable().setRowSelectionInterval(r, r);
		} else {
			panel.getTable().clearSelection();
		}
		if (!newIssue && (r > -1)) {
			new IssueView(
					(int) panel.getTable().getModel()
							.getValueAt(panel.getTable().getRowSorter().convertRowIndexToModel(r), 0),
					currentUser, iTracker, false);
		} else {
			new IssueView(-1, currentUser, iTracker, false);

		}

	}

	/*
	 * Updates the search panel with the specified issue id from the ID search
	 */
	public static void SearchIssuesForSpecificId() {

		try {
			if (!gui.getSearchByIDField().getText().isEmpty()) {
				// text	ID parsed as a local int variable.
				int tempInt = Integer.parseInt(gui.getSearchByIDField().getText()); 
				// checks to see if ID is between 0 and all issues or equal to.
				if (tempInt > 0 && tempInt <= iTracker.getIssueList().size()) { 
					// updates the panel and finds the issue by id.
					gui.getsearchDPanel().update(iTracker.getIssueByID(tempInt));
					return;
				}

			}
		} catch (NumberFormatException e1) {

			JOptionPane.showMessageDialog(gui, "Numbers Only!");
		}
		gui.getsearchDPanel().removeAllRows();

	}

	/**
	 * Updates the home panels with the current users open and new issues
	 */
	public static void updateCurrentUserIssues() {

		LocalDate tenDaysAgo = LocalDate.now().minusDays(10);
		LocalDate today = LocalDate.now();
		gui.getUserNewestIssuesPanel().update(iTracker.getIssuesByUserAndDate(currentUser, tenDaysAgo, today));

		gui.getUserUnresolvedIssuesPanel().update(iTracker.getUnresolvedIssueByUser(currentUser));

	}

	/**
	 * void method that takes no parameters. method for all actionlisteners and
	 * mouse events.
	 */
	private static void addActionListeners() {
		gui.getBtnGetPriority().addActionListener(new guiEvent());
		gui.getBtnNewIssue().addActionListener(new guiEvent());
		gui.getBtnUpdate().addActionListener(new guiEvent());
		gui.getMenuImportData().addActionListener(new guiEvent());
		gui.getMenuSettingsAddUser().addActionListener(new guiEvent());
		gui.getTabbedPane().addChangeListener(new guiChangeEvent());
		gui.getAllIssuesPanel().getTable().addMouseListener(new guiMouseEvent());
		gui.getsearchDPanel().getTable().addMouseListener(new guiMouseEvent());
		gui.getUserNewestIssuesPanel().getTable().addMouseListener(new guiMouseEvent());
		gui.getUserUnresolvedIssuesPanel().getTable().addMouseListener(new guiMouseEvent());
		gui.getUserList().addMouseListener(new guiMouseEvent());
		gui.getSearchByIDField().getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				// runs if change in field
				SearchIssuesForSpecificId();

			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				// runs if change in field
				SearchIssuesForSpecificId();

			}

		});
	}

}
