// Class for stored lists of priority and status.

package no.uib.info233.v2016.sto020_can013.oblig3.utilities;

public  class Globals {
	 // list of priority options for issues.
		public  final static String[] priorityList = {"All", "None", "Low", "Normal", "High", "Critical" };
		// list of different statuses for issues.
		public  final static String[] statusList = { "Received", "Assigned Employee", "Waiting for Customer", 
				"Waiting for Employee", "Solved" };
}
