/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig3.utilities;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import no.uib.info233.v2016.sto020_can013.oblig3.XML.XMLBuilder;
import no.uib.info233.v2016.sto020_can013.oblig3.XML.XMLIssueHandler;
import no.uib.info233.v2016.sto020_can013.oblig3.XML.XMLReader;
import no.uib.info233.v2016.sto020_can013.oblig3.XML.XMLUserHandler;
import no.uib.info233.v2016.sto020_can013.oblig3.issuetracker.IssueTracker;

/**
 * A Utility class mostly for converting strings
 * 
 * @author sto020
 *
 */
public class Utils {

	private static File fileIssues = new File(".\\issues.xml");
	private static File fileUsers = new File(".\\users.xml");
	private static File fileIssuesArchive = new File(".\\issues-archive.xml");

	/**
	 * Loads data from XML files if exists
	 * 
	 * @return boolean - if load successful
	 * 
	 */
	public static IssueTracker loadData() {
		IssueTracker load = new IssueTracker();

		try {

			// Load users FIRST
			if (fileUsers.exists()) {
				System.out.println("Loading users to IssueTracker");

				XMLReader.readXML(fileUsers, new XMLUserHandler(load));
			} else {
				System.out.println("users.xml does not exist!");
			}

			// Load issues SECOND
			if (fileIssues.exists()) {
				System.out.println("Loading issues to IssueTracker");

				XMLReader.readXML(fileIssues, new XMLIssueHandler(load));
			} else {
				System.out.println("issue.xml does not exist!");
			}

		} catch (Exception ex) {
			System.out.println("Error reading saved issues and users!");

		}

		return load;

	}

	/**
	 * Saves the data to xml
	 * 
	 * @param issueTracker
	 *            the IssueTracker object
	 * @return boolean - if saved successfully or not
	 */
	public static boolean saveData(IssueTracker issueTracker,boolean saveArchivedOnly) {

		// serialize the Queue
		System.out.println("Saving data to xml");
		try {
			if(!saveArchivedOnly)
			{
			XMLBuilder.issueBuilder(issueTracker.getIssueList(), fileIssues);
			XMLBuilder.userBuilder(issueTracker.getUserList(), fileUsers);
			}
			else
			{
				XMLBuilder.issueBuilder(issueTracker.getIssueListByStatus("Solved"), fileIssuesArchive);

			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	
	/**
	 * This method will convert a String input of Numbers to an int
	 * 
	 * @param value
	 *            The string of numbers to convert to int
	 * @return The parsed string converted to int, will return 0 if error
	 */
	public static int StringToInteger(String value) {

		try {
			return Integer.parseInt(value.trim());

		} catch (NumberFormatException nfe) {
			System.out.println("Error parsing String to Integer. Please check input, returning '0'");
			return 0;
		}

	}

	/**
	 * Creates a MDF hash of a string input, used for saving and comparing
	 * passwords
	 * 
	 * @param value
	 *            The String/Password to create hash of
	 * @param salt
	 *            - extra salt to create a spicy hash
	 * @return The finished hashed password for usage and saving
	 */
	public static String StringHash(String value, String salt) {
		String finalsalt = "thisisnotthesaltyourlookingfor" + salt + "312pewpew12@$@4&#%^$*";
		value = value + finalsalt;
		try {
			// Create an instance for MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			// Add password bytes to the hash creator
			md.update(value.getBytes());
			// Get the bytes from the hash
			byte[] bytes = md.digest();
			// Convert the byte hash to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			// return the hashed password in hex format
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return null;

	}

	/*
	 * 
	 */
	public static LocalDate StringToLocalDate(String dateString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("M/dd/yyyy");
		DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("M/d/yyyy");
		DateTimeFormatter formatter4 = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		LocalDate date = null;

		try {
			date = LocalDate.parse(dateString, formatter);

		} catch (DateTimeParseException ex) {
			try {
				date = LocalDate.parse(dateString, formatter2);

			} catch (DateTimeParseException e) {
				try {
					date = LocalDate.parse(dateString, formatter3);

				} catch (DateTimeParseException e2) {

					try {
						date = LocalDate.parse(dateString, formatter4);

					} catch (DateTimeParseException e3) {

						System.out.println(e3.getMessage());

					}

				}
			}
		}

		return date;
	}

}
