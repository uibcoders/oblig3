/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig3.events;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import no.uib.info233.v2016.sto020_can013.oblig3.gui.GUI;
import no.uib.info233.v2016.sto020_can013.oblig3.main.Main;

/**
 * Class representing the changeEvents for the GUI
 * @author sto020
 *@version 0.1.0
 */
public class guiChangeEvent implements ChangeListener {

	private GUI gui;
	
	/**
	 * Constructor for the Event class
	 * @param gui
	 */
	public guiChangeEvent(){
		this.gui = GUI.getInstance();
	}
	
	
	@Override
	public void stateChanged(ChangeEvent e) {
		
		if(e.getSource() == gui.getTabbedPane()){
		if (gui.getTabbedPane().getSelectedIndex() == 1) {
			Main.updateAllIssueLists();
		}
		}
	}

}
