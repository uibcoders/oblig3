package no.uib.info233.v2016.sto020_can013.oblig3.events;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import no.uib.info233.v2016.sto020_can013.oblig3.gui.GUI;
import no.uib.info233.v2016.sto020_can013.oblig3.main.Main;


/**
 * Class representing an event for a gui object
 * 
 * @author sto020
 * @version 0.1.0
 *
 */
public class guiEvent implements ActionListener {

	private GUI gui;

	/**
	 * Constructor for the Event class
	 * 
	 * @param gui
	 */
	public guiEvent() {
		this.gui = GUI.getInstance();

	}

	/**
	 * Method to respond on a user event
	 * 
	 * @param event
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		if (event.getSource() == gui.getBtnGetPriority()) {
			Main.searchPriority();
		} else if (event.getSource() == gui.getBtnNewIssue()) {
			Main.editIssue(true, gui.getAllIssuesPanel());
		}  else if (event.getSource() == gui.getMenuSettingsAddUser()) {
			Main.editUser(true);
		} else if (event.getSource() == gui.getBtnUpdate()) {
			Main.updateSearchList();

		} else if (event.getSource() == gui.getMenuImportData()) {

			Main.importFromXML();

		}

	}

}
