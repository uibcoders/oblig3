/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig3.issuetracker;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Handles the collection of issues and Users and logic to add new ones
 * 
 * @author sto020
 * @author can013
 *
 */
public class IssueTracker implements Serializable {

	private static final long serialVersionUID = -5848946530947998962L;
	HashMap<String, User> userList = new HashMap<String, User>();
	List<Issue> issueList = new ArrayList<Issue>(5000);

	/**
	 * Constructor for the IssueTracker class
	 */
	public IssueTracker() {
	}

	/**
	 * Adds a user based on the username, creates an ID based on the collection
	 * of Users If the user exist it will not be created but returned
	 * 
	 * @param username
	 *            A great username for the User
	 * @return The newly created or existing User
	 */
	public User addUser(String username) {

		if (!checkUserExist(username)) {

			userList.put(username, new User((userList.size() + 1), username));
		}

		return getUser(username);
	}

	/**
	 * Adds a already created user to the collection of Users
	 * 
	 * @param user
	 *            The user to add
	 */
	public User addUser(User user) {

		userList.put(user.getUsername(), user);
		return getUser(user.getUsername());

	}

	/**
	 * Gets a User based on the username
	 * 
	 * @param username
	 *            The username to search for
	 * 
	 * @return The user object if exist, or null
	 */
	public User getUser(String username) {
		return userList.get(username);

	}

	/*
	 * Removes a user from the collection
	 * 
	 * @param user The user object to remove from collection
	 */
	public boolean removeUser(User user) {
		return (!checkUserExist(user.getUsername())) ? true : userList.remove(user.getUsername(), user);

	}

	/**
	 * Checks if the user exist in the collection
	 * 
	 * @param username
	 *            The username to check if exist
	 * 
	 * @return true if exist
	 */
	public boolean checkUserExist(String username) {
		return userList.containsKey(username);

	}

	/**
	 * Adds an Issue to the collection of issues, will not be added if it exist
	 * 
	 * @param ID
	 *            - The issue ID
	 * @param priority
	 *            - Priority of the issue
	 * @param assigned_user
	 *            - The User assigned to the issue
	 * @param createdDate
	 *            - The date this issue was created, "01/12/2016"
	 * @param descriptiveText
	 *            - A description of the issue
	 * @param location
	 *            - The Location of the issue
	 * @param status
	 *            - The status of the issue
	 * @param createdBy
	 *            - The creator of the issue
	 * @param changedBy
	 *            - The user who changed the issue
	 * @return false if the issue exist
	 */
	public boolean addIssue(int ID, String priority, User assigned_user, LocalDate createdDate, String descriptiveText,
			String location, String status, String createdBy, String changedBy) {
		Issue issue = new Issue(ID, priority, assigned_user, createdDate, descriptiveText, location, status, createdBy,
				changedBy);
		if (!issueList.contains(issue)) {
			issueList.add(issue);

			return true;
		} else {
			System.out.println("Issue Already exist!");

			return false;
		}
	}

	/*
	 * Removes all issues from the collection
	 */
	public void clearIssueList() {

		this.issueList.clear();

	}

	/**
	 * Gets a list of High priority issues as long as priority string equals
	 * critical
	 * 
	 * @param priority
	 *            - The priority to return
	 * @return A list of the selected priority Issues
	 */
	public List<Issue> getPriorityIssues(String priority) {
		List<Issue> tmpList = new ArrayList<Issue>();
		for (Issue issue : issueList) {
			if (issue.getPriority().equals(priority))
				tmpList.add(issue);
		} // end of for loop
		Collections.sort(tmpList, new IssueIDComparator());

		return tmpList;

	}

	/**
	 * Gets all issues assigned to a username
	 * 
	 * @param username
	 *            The username to get assigned issues
	 * 
	 * @return A collection of issues matching the username
	 */
	public List<Issue> getIssuesByUser(String username) {
		List<Issue> tmpList = new ArrayList<Issue>();
		for (Issue issue : issueList) {
			if (issue.getAssignedUser() != null) {
				if (issue.getAssignedUser().getUsername().equals(username)) {

					tmpList.add(issue);
				}
			}
		}
		Collections.sort(tmpList, new IssueIDComparator());

		return tmpList;
	}

	/**
	 * Gets all issues assigned to a username which are not solved yet
	 * 
	 * @param username
	 *            The username to get unresolved issue
	 * 
	 * @return A collection of issues matching the username and are not solved
	 */
	public List<Issue> getUnresolvedIssueByUser(String username) {
		List<Issue> tmpList = new ArrayList<Issue>();
		for (Issue issue : getIssuesByUser(username)) {

			if (!issue.getStatus().equals("Solved")) {

				tmpList.add(issue);
			}
		}
		Collections.sort(tmpList, new IssueIDComparator());

		return tmpList;
	}

	/**
	 * Gets a collection of issues assigned to a specified username, and between
	 * two dates
	 * 
	 * @param username
	 *            The username to get assigned issues
	 * @param minDate
	 *            - The minimum date of issues
	 * @param maxDate
	 *            - The maximum date of issues
	 * @return A collection of issues matching the criteria
	 */
	public List<Issue> getIssuesByUserAndDate(String username, LocalDate minDate, LocalDate maxDate) {
		List<Issue> tmpList = new ArrayList<Issue>();
		for (Issue issue : getIssuesByUser(username)) {

			if (isIssueBetweenDates(issue, minDate, maxDate)) {
				tmpList.add(issue);
			}

		}
		Collections.sort(tmpList, new IssueIDComparator());

		return tmpList;
	}

	/**
	 * Gets a collection of issues assigned to a specified username, and between
	 * two dates
	 * 
	 * @param username
	 *            The username to get assigned issues
	 * @param minDate
	 *            - The minimum date of issues
	 * @param maxDate
	 *            - The maximum date of issues
	 * @param solved
	 *            - Get solved issues?
	 * @return A collection of issues matching the criteria
	 */
	public List<Issue> getIssuesByUserAndDate(String username, LocalDate minDate, LocalDate maxDate, boolean solved) {
		List<Issue> tmpList = new ArrayList<Issue>();
		for (Issue issue : getIssuesByUser(username)) {
			if (solved && issue.getStatus().equals("Solved")) {

				if (isIssueBetweenDates(issue, minDate, maxDate)) {
					tmpList.add(issue);
				}
			} else if (!solved && !issue.getStatus().equals("Solved")) {
				{
					if (isIssueBetweenDates(issue, minDate, maxDate)) {
						tmpList.add(issue);
					}
				}
			}

		}
		Collections.sort(tmpList, new IssueIDComparator());

		return tmpList;
	}

	/**
	 * Gets a collection of issues between two dates
	 * 
	 * @param minDate
	 *            - The minimum date of issues
	 * @param maxDate
	 *            - The maximum date of issues
	 * @return A collection of issues matching the criteria
	 */
	public List<Issue> getIssuesBetweenDates(LocalDate minDate, LocalDate maxDate) {

		List<Issue> tmpList = new ArrayList<Issue>();
		for (Issue issue : issueList) {

			if (isIssueBetweenDates(issue, minDate, maxDate)) {
				tmpList.add(issue);
			}
		}
		Collections.sort(tmpList, new IssueIDComparator());

		return tmpList;

	}

	/**
	 * Gets a collection of issues between two dates
	 * 
	 * @param minDate
	 *            - The minimum date of issues
	 * @param maxDate
	 *            - The maximum date of issues
	 * @param solved
	 *            - Get solved issues?
	 * @return A collection of issues matching the criteria
	 */
	public List<Issue> getIssuesBetweenDates(LocalDate minDate, LocalDate maxDate, boolean solved) {

		List<Issue> tmpList = new ArrayList<Issue>();
		for (Issue issue : issueList) {
			if (solved && issue.getStatus().equals("Solved")) {

				if (isIssueBetweenDates(issue, minDate, maxDate)) {
					tmpList.add(issue);
				}
			} else if (!solved && !issue.getStatus().equals("Solved")) {
				if (isIssueBetweenDates(issue, minDate, maxDate)) {
					tmpList.add(issue);
				}
			}

		}
		Collections.sort(tmpList, new IssueIDComparator());

		return tmpList;

	}

	/**
	 * Get the list of issues by status key
	 * 
	 * @param status
	 *            -the status to get
	 * @return The List of Issue objects
	 */
	public List<Issue> getIssueListByStatus(String status) {

		List<Issue> tmpList = new ArrayList<Issue>();
		for (Issue issue : issueList) {
			if (issue.getStatus().equals(status)) {
				tmpList.add(issue);
			}
		}

		Collections.sort(tmpList, new IssueIDComparator());

		return tmpList;

	}

	/**
	 * Checks if an issue is between two dates
	 * 
	 * @param issue
	 *            - The issue object to check
	 * @param minDate
	 *            - The minimum date the issue can be
	 * @param maxDate
	 *            - The maximum date the issue can be
	 * @return true if the issue is between the dates
	 */
	private boolean isIssueBetweenDates(Issue issue, LocalDate minDate, LocalDate maxDate) {
		if (issue.getCreatedDate().isAfter(minDate.minusDays(1))
				&& issue.getCreatedDate().isBefore(maxDate.plusDays(1))) {
			return true;
		}

		return false;
	}

	/**
	 * Remove an issue from the collection of issues
	 * 
	 * @param issue
	 *            The issue to remove from the collection
	 * 
	 * @return true if the issue was removed or does not exist
	 */
	public boolean removeIssue(Issue issue) {
		if (!issueList.contains(issue))
			return true;
		return issueList.remove(issue);

	}

	/**
	 * Gets an issue based on its ID
	 * 
	 * @param ID
	 *            The id to find
	 * 
	 * @return The issue based on the ID
	 */
	public Issue getIssueByID(int ID) {
		Issue tmpIssue = null;
		for (Issue issue : issueList) {
			if (issue.getID() == ID)
				tmpIssue = issue;
		}
		return tmpIssue;
	}

	/**
	 * Will List all issues saved to console
	 */
	public void printIssues() {
		for (Issue issue : issueList) {
			System.out.println(issue);
		}
	}

	/**
	 * Will List all Users saved to console
	 */
	public void printUsers() {
		for (String key : userList.keySet()) {
			System.out.println(userList.get(key));
		}
	}

	/**
	 * Get the complete list of users
	 * 
	 * @return The HashMap of User objects
	 */
	public HashMap<String, User> getUserList() {
		return userList;

	}

	/**
	 * Gets a alphabetically sorted list of users
	 * 
	 * @return sortUsers A list of sorted users
	 */
	public List<User> getSortedUsers() {
		List<User> sortUsers = new ArrayList<User>(userList.values());
		Collections.sort(sortUsers, new UsernameComparator());

		return sortUsers;

	}

	/**
	 * Get the complete list of issues
	 * 
	 * @return The List of Issue objects
	 */
	public List<Issue> getIssueList() {
		Collections.sort(issueList, new IssueIDComparator());

		return issueList;

	}

	/**
	 * Used to sort the collection of issues so the id will always be in the
	 * correct order
	 */
	private class IssueIDComparator implements Comparator<Issue> {
		@Override
		public int compare(Issue i1, Issue i2) {
			return i1.getID() - i2.getID();
		}
	}

	/**
	 * Used to sort the collection of users so the username will always be in
	 * the correct order
	 */
	public class UsernameComparator implements Comparator<User> {
		@Override
		public int compare(User u1, User u2) {
			return u1.getUsername().compareTo(u2.getUsername());
		}
	}

}
