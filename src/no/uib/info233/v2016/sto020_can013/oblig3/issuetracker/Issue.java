/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig3.issuetracker;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * Represents an Issue
 * 
 * @author sto020
 * @author can013
 *
 */
public class Issue implements Serializable {

	private static final long serialVersionUID = 2932297150690670593L;
	int ID;

	String priority = null; // the priority element in the issue.
	User assignedUser = null; // the assigned user to the issue.
	LocalDate createdDate = null; // the date the issue was created.
	String descriptiveText = null; // description of the issue.
	String location = null; // location of the issue.
	String status = null;
	String createdBy = null;
	String changedBy = null;

	/**
	 * Creates an Issue object
	 * 
	 * @param ID
	 *            The issue id
	 * @param priority
	 *            The priority of the issue
	 * @param assignedUser
	 *            The assigned Employee/User
	 * @param createdDate
	 *            The date the issue was created
	 * @param savedText
	 *            The issue descriptive text
	 * @param location
	 *            The location of the Issue
	 * @param status
	 *            The status of the issue
	 * @param createdBy
	 *            The creator of the issue
	 * @param changedBy
	 *            The last user who changed the issue
	 */
	public Issue(int ID, String priority, User assignedUser, LocalDate createdDate, String descriptiveText,
			String location, String status, String createdBy, String changedBy) {
		this.ID = ID;
		this.priority = priority;
		this.assignedUser = assignedUser;
		this.createdDate = createdDate;
		this.descriptiveText = descriptiveText;
		this.location = location;
		this.status = status;
		this.createdBy = createdBy;
		this.changedBy = changedBy;
	}

	public Issue() {

	}

	/*
	 * Overrides the toString of Issue to give a nice string in return
	 */
	@Override
	public String toString() {
		return String.format("Issue %s, %s by %s: Priority %s, %s. %s. %s. Status: %s - Last edited by: %s\n", this.ID, this.createdDate,
				this.createdBy, this.priority, this.descriptiveText, this.location, this.assignedUser, this.status , this.changedBy);

	}

	/**
	 * Get the id number for the issue
	 * 
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * Set the id number for the issue
	 * 
	 * @param iD
	 *            the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * @return the priority
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * @param priority
	 *            the priority to set
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * Get the User assigned to fix the issue
	 * 
	 * @return the assignedUser
	 */
	public User getAssignedUser() {
		return assignedUser;
	}

	/**
	 * Set the User assigned to fix the issue
	 * 
	 * @param assignedUser
	 *            the assignedUser to set
	 */
	public void setAssignedUser(User assignedUser) {
		this.assignedUser = assignedUser;
	}

	/**
	 * Get the date this issue was created
	 * 
	 * @return the createdDate
	 */
	public LocalDate getCreatedDate() {
		return createdDate;
	}

	/**
	 * Set the date this issue was created
	 * 
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Get the descriptive text of the issue
	 * 
	 * @return the savedText
	 */
	public String getDescriptiveText() {
		return descriptiveText;
	}

	/**
	 * Set a descriptive text for the issue
	 * 
	 * @param savedText
	 *            the savedText to set
	 */
	public void setDescriptiveText(String descriptiveText) {
		this.descriptiveText = descriptiveText;
	}

	/**
	 * Get the location of the issue
	 * 
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Set the location of the issue
	 * 
	 * @param location
	 *            the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Gets the status of the issue, last user and time
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status of the issue, last user and time
	 * 
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the user who created the issue
	 * 
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the user who created the issue
	 * 
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the user who changed the issue
	 * 
	 * @return the changedBy
	 */
	public String getChangedBy() {
		return changedBy;
	}

	/**
	 * Sets the user who changed the issue
	 * 
	 * @param changedBy
	 *            the changedBy to set
	 */
	public void setChangedBy(String changedBy) {
		this.changedBy = changedBy;
	}



}
