package no.uib.info233.v2016.sto020_can013.oblig3.issuetracker;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/**
 * A Custom model to be used with JTables, uses the AbstractTableModel
 * 
 * @author sto020
 * @author can013
 */

public class IssueModel extends AbstractTableModel {
	// Array of names for the columns
	String[] columnNames = { "ID", "Priority", "Creation Date", "Description", "Location", "Assigned To", "Status",
			"Created By", "Changed By" }; // Array

	private static final long serialVersionUID = 1L;

	private List<Issue> issues; // The array list of all issues.

	/**
	 * Constructor for the IssueModel class
	 * 
	 * @param issueList
	 *            the list of issues to be set to field array list.
	 */
	public IssueModel(List<Issue> issueList) {
		this.issues = issueList;
	}

	/*
	 * @return the number of rows in the array.
	 */
	@Override
	public int getRowCount() {
		return this.issues == null ? 0 : this.issues.size();
	}

	/*
	 * @return 9 the static column count.
	 */
	@Override
	public int getColumnCount() {
		return 9;
	}
	
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {

		switch (columnIndex) {
		case 0:
			return Integer.class; // ID
		case 1:
			return String.class;// Priority

		case 2:
			return LocalDate.class;// Created
		case 3:
			return String.class;// Description
		case 4:
			return String.class;// Location

		case 5:
			return String.class;// User
		case 6:
			return String.class;// Status
		case 7:
			return String.class;// created by
		case 8:
			return String.class;// changed by
		default:
			return String.class; // Default
		}
	}

	/*
	 * @param col the number of columns.
	 * 
	 * @return
	 */
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/*
	 * @param rowIndex the index of the row.
	 * 
	 * @param columnIndex the index of the column. returns the value at the
	 * selected rowIndex and columnIndex.
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		Object value = null;
		Issue issue = issues.get(rowIndex);
		switch (columnIndex) {
		case 0:
			value = issue.getID();
			break;
		case 1:
			value = issue.getPriority();
			break;
		case 2:
			value = ((LocalDate) issue.getCreatedDate()).toString();
			break;
		case 3:
			value = issue.getDescriptiveText();
			break;
		case 4:
			value = issue.getLocation();
			break;
		case 5:
			if (issue.getAssignedUser() != null) {
				value = issue.getAssignedUser().getUsername();
			}
			break;
		case 6:
			value = issue.getStatus();
			break;
		case 7:
			value = issue.getCreatedBy();
			break;
		case 8:
			value = issue.getChangedBy();
			break;
		}
		return value;
	}


	/**
	 * @param index the index of desired issue ID.
	 * @return the ID of issue at specified index.
	 */
	public int getIssueIdAt(int index) {
		return Integer.parseInt((String) getValueAt(index, 0));
	}

	/**
	 * @param row
	 *            the row to remove. method removes the selected row.
	 */
	public void removeRow(int row) {
		if (issues == null)
			return;
		issues.remove(row);
	}

	/**
	 * @param issue
	 *            the issue to add to a new row. method adds a new issue to an
	 *            available row.
	 */
	public void addRow(Issue issue) {
		if (issues == null)
			this.issues = new ArrayList<Issue>();

		issues.add(issue);
	}
}