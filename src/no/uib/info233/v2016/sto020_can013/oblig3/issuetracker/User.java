/**
 * 
 */
package no.uib.info233.v2016.sto020_can013.oblig3.issuetracker;

import java.io.Serializable;

import no.uib.info233.v2016.sto020_can013.oblig3.utilities.Utils;

/**
 * Represents a User
 * 
 * @author sto020
 *
 */
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5915288619782080184L;
	private int ID;
	private String firstName = null;
	private String lastName = null;
	private String username = null;
	private String loginPasswordHash = null;

	/**
	 * Constructor
	 */
	public User() {

	}

	/**
	 * Constructor for the user class, creates a minimal user with only id and
	 * username
	 * 
	 * @param ID
	 *            The id of the user
	 * @param username
	 *            The username of this user
	 */
	public User(int ID, String username) {
		this.ID = ID;
		this.username = username;

	}

	/**
	 * Overrides the toString() to create a custom output string fitted to the
	 * user
	 * 
	 * @return the Custom string
	 */
	@Override
	public String toString() {
		return String.format("(User %s: %s - %s %s)", this.ID, this.username, this.firstName, this.lastName);
	}

	/**
	 * @return the iD
	 */
	public int getID() {
		return ID;
	}

	/**
	 * @param iD
	 *            the iD to set
	 */
	public void setID(int iD) {
		ID = iD;
	}

	/**
	 * Get the username of the User
	 * 
	 * @return the username of the User
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Set/Change the username of the User
	 * 
	 * @param name
	 *            the username of the User
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Get the loginPassword
	 * 
	 * @return the loginPassword
	 */
	public String getLoginPasswordHash() {
		return loginPasswordHash;
	}

	/**
	 * Create a password for the User, will make a MD5 hash of the string input
	 * 
	 * @param loginPassword
	 *            the loginPassword to set
	 */
	public void setLoginPassword(String loginPassword) {
		this.loginPasswordHash = Utils.StringHash(loginPassword,this.username);
	}

	/**
	 * Add a MD5 password hash for the User
	 * 
	 * @param loginPasswordHash
	 *            the loginPasswordHash to set
	 */
	public void setLoginPasswordHash(String loginPasswordHash) {
		this.loginPasswordHash = loginPasswordHash;
	}

	/**
	 * Get the first name of the user
	 * 
	 * @return User first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Set the first name of the user
	 * 
	 * @param firstName
	 *            User first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Get the last name of the user
	 * 
	 * @return User last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Set the last name of the user
	 * 
	 * @param lastName
	 *            User last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}